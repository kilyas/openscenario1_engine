################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building yase with Conan
################################################################################

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import rm 
from conans import tools, CMake
import os

required_conan_version = ">=1.47.0"

class YaseConan(ConanFile):
    name = "Yase"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False], "Yase_BUILD_TESTS":[True, False], "cmake_generator": ["MSYS Makefiles", "Unix Makefiles", "Visual Studio", "Ninja"]}
    default_options = {"shared": True, "fPIC": False, "Yase_BUILD_TESTS":False}
    exports_sources = "*"
    short_paths = True
    no_copy_source = False

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "MSYS Makefiles")
        else:
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "Unix Makefiles")
        
    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version
        
        return url, sha256
        
    def source(self): 
        url, sha256 = self._get_url_sha()
        git = tools.Git(folder=self.name)
        git.clone(url)
        git.checkout(sha256)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_GENERATOR"] = self.options.cmake_generator
        cmake.definitions["Yase_BUILD_TESTS"] = self.options.Yase_BUILD_TESTS
        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        if self.settings.os == "Windows":
            cmake = CMake(self, generator="MSYS Makefiles")
        else:
            cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "Yase")
        self.cpp_info.set_property("cmake_target_name", "Yase::agnostic_behavior_tree")
    
        