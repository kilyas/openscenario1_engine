################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building mantle api with Conan
################################################################################

from conans import tools, AutoToolsBuildEnvironment, CMake, ConanFile
from conan.tools.files import rm 
import os

required_conan_version = ">=1.47.0"

class MantleAPIConan(ConanFile):
    name = "MantleAPI"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], 
               "fPIC": [True, False], 
               "MantleAPI_TEST":[True, False],
               "cmake_generator": ["MSYS Makefiles", "Unix Makefiles", "Visual Studio", "Ninja"]}

    default_options = {"shared": False, 
                       "fPIC": True, 
                       "MantleAPI_TEST":False}

    exports_sources = "*"
    no_copy_source = False
    generators = "cmake"
    short_paths = True

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "MSYS Makefiles")
        else:
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "Unix Makefiles")
        
    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version
        
        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = tools.Git(folder=self.name)
        git.clone(url)
        git.checkout(sha256)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_GENERATOR"] = self.options.cmake_generator
        cmake.definitions["MantleAPI_TEST"] = self.options.MantleAPI_TEST
        cmake.configure(source_folder=self.name)
        cmake.build()


    def package(self):
        cmake = CMake(self)
        cmake.install()
        self.copy("*", src=os.path.join(self.name, "test/MantleAPI"), dst="test/MantleAPI")
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
    
        