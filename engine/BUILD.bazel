load("@rules_cc//cc:defs.bzl", "cc_library", "cc_test")

################################################################################
# Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

cc_library(
    name = "open_scenario_engine",
    srcs = glob([
        "src/**/*.cpp",
        "gen/**/*.cpp",
    ]),
    hdrs = glob([
        "gen/**/*.h",
        "include/**/*.h",
        "src/**/*.h",
    ]),
    includes = [
        "gen",
        "include",
        "src",
    ],
    visibility = ["//visibility:public"],
    deps = [
        "@mantle_api",
        "@open_scenario_parser",
        "@units_nhh",
        "@yase//middle_end/agnostic_behavior_tree",
    ],
)

cc_test(
    name = "convert_osc_to_mantle_tests",
    timeout = "short",
    srcs = glob(["tests/Conversion/**/*.cpp"]),
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "open_scenario_engine_test",
    timeout = "short",
    srcs = ["tests/OpenScenarioEngineTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

filegroup(
    name = "open_scenario_engine_test_data",
    data = glob(["tests/data/**/*.*"]),
    visibility = ["//visibility:public"],
)

cc_library(
    name = "open_scenario_builders",
    srcs = glob(["tests/builders/*.cpp"]),
    hdrs = glob(["tests/builders/*.h"]),
    includes = ["tests"],
    deps = ["@open_scenario_parser"],
)

cc_library(
    name = "open_scenario_engine_test_utils",
    hdrs = ["tests/TestUtils.h"],
    includes = ["tests"],
    linkopts = ["-lstdc++fs"],
    visibility = ["//visibility:public"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "storyboard_by_entity_condition_tests",
    timeout = "short",
    srcs = glob(["tests/Storyboard/ByEntityCondition/*.cpp"]),
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_logger",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "storyboard_by_value_condition_tests",
    timeout = "short",
    srcs = glob(["tests/Storyboard/ByValueCondition/*.cpp"]),
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "storyboard_generic_action_tests",
    timeout = "short",
    srcs = glob(["tests/Storyboard/GenericAction/*.cpp"]),
    tags = ["test"],
    deps = [
        ":mock_probability_service",
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "storyboard_motion_control_action_tests",
    timeout = "short",
    srcs = glob(["tests/Storyboard/MotionControlAction/*.cpp"]),
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_logger",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "traffic_signal_parsing_tests",
    timeout = "short",
    srcs = glob(["tests/TrafficSignalParsing/*.cpp"]),
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "utils_tests",
    timeout = "short",
    srcs = glob(["tests/Utils/*.cpp"]),
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "node_tests",
    timeout = "short",
    srcs = glob([
        "tests/Node/*.cpp",
        "tests/Node/**/*.h",
    ]),
    includes = ["tests"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_library(
    name = "open_scenario_engine_test_logger",
    hdrs = [
        "tests/TestUtils/MockLogger.h",
        "tests/TestUtils/TestLogger.h",
    ],
    includes = ["tests"],
)

cc_library(
    name = "mock_probability_service",
    hdrs = ["tests/TestUtils/MockProbabilityService.h"],
    includes = ["tests"],
)
