/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "builders/ActionBuilder.h"
#include "Storyboard/MotionControlAction/LaneChangeAction.h"
#include "Storyboard/MotionControlAction/LaneChangeAction_impl.h"
#include "TestUtils.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::Return;
using namespace OpenScenarioEngine::v1_2;
using namespace testing::OpenScenarioEngine::v1_2;

class LaneChangeActionTestFixture : public OpenScenarioEngineLibraryTestBase {
protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();
        auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
        auto entity_broker = std::make_shared<EntityBroker>(false);
        entity_broker->add("Ego");
        root_node_ = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
    }

    std::shared_ptr<FakeRootNode> root_node_{nullptr};
};

#if !defined (WIN32) && !defined (WIN64)
TEST_F(LaneChangeActionTestFixture, GivenLaneChangeActionWithoutDynamics_WhenDistributeData_SegmentationFault)
{
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
                    .WithLaneChangeTarget(
                            FakeLaneChangeTargetBuilder{}.WithAbsoluteTargetLane(
                                            FakeAbsoluteTargetLaneBuilder{}.WithValue("1").Build())
                                    .Build())
                    .Build();
    auto lane_change_action = std::make_shared<Node::LaneChangeAction>(lane_change_action_data);

    root_node_->addChild(lane_change_action);
    ASSERT_EXIT(root_node_->distributeData(), ::testing::KilledBySignal(SIGSEGV),".*");
}
#endif

TEST_F(LaneChangeActionTestFixture, GivenLaneChangeActionWithoutLaneChangeTarget_WhenExecuteTick_ExceptionRaised)
{
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
                    .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
                    .WithLaneChangeTarget(FakeLaneChangeTargetBuilder{}.Build())
                    .Build();
    auto lane_change_action = std::make_shared<Node::LaneChangeAction>(lane_change_action_data);

    root_node_->addChild(lane_change_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_ANY_THROW(root_node_->executeTick());
}

TEST_F(LaneChangeActionTestFixture,
       GivenLaneChangeActionWithAbsoluteTargetLane_WhenControlStrategyGoalReached_ThenExpectCorrectStrategy)
{
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_2::DynamicsShape dynamics_shape(shape_name);
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
            .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
            .WithLaneChangeTarget(
                    FakeLaneChangeTargetBuilder{}.WithAbsoluteTargetLane(
                            FakeAbsoluteTargetLaneBuilder{}.WithValue("1").Build())
                                .Build())
            .Build();
    auto lane_change_action = std::make_shared<Node::LaneChangeAction>(lane_change_action_data);

    auto expected_control_strategy = mantle_api::PerformLaneChangeControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
            .Times(1)
            .WillRepeatedly([expected_control_strategy](
                    std::uint64_t controller_id,
                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = controller_id;
                EXPECT_EQ(1, control_strategies.size());
                auto control_strategy = dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, control_strategy);
                EXPECT_EQ(*control_strategy, expected_control_strategy);
            });

    root_node_->addChild(lane_change_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LaneChangeActionTestFixture,
       GivenLaneChangeActionWithRelativeLaneTarget_WhenControlStrategyGoalReached_ThenExpectCorrectStrategy)
{
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_2::DynamicsShape dynamics_shape(shape_name);
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
            .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
            .WithLaneChangeTarget(
                    FakeLaneChangeTargetBuilder{}.WithRelativeTargetLane(
                            FakeRelativeTargetLaneBuilder{}.WithValue(1).WithEntityRef("Ego").Build())
                                .Build())
            .Build();
    auto lane_change_action = std::make_shared<Node::LaneChangeAction>(lane_change_action_data);

    auto expected_control_strategy = mantle_api::PerformLaneChangeControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
            .Times(1)
            .WillRepeatedly([expected_control_strategy](
                    std::uint64_t controller_id,
                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = controller_id;
                EXPECT_EQ(1, control_strategies.size());
                auto control_strategy = dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, control_strategy);
                EXPECT_EQ(*control_strategy, expected_control_strategy);
            });

    root_node_->addChild(lane_change_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST(LaneChangeAction, GivenMatchingControlStrategyGoalIsReached_ReturnTrue)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kPerformLaneChange))
      .WillByDefault(Return(true));

  OpenScenarioEngine::v1_2::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               OpenScenarioEngine::v1_2::TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  ASSERT_TRUE(laneChangeAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(LaneChangeAction, GivenUnmatchingControlStrategyGoalIsReached_ReturnsFalse)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  OpenScenarioEngine::v1_2::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               OpenScenarioEngine::v1_2::TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  ASSERT_FALSE(laneChangeAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(LaneChangeAction, GivenControlStrategyElements_SetUpControlStrategy)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};

  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(0, testing::_))
      .Times(1)
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_2::LaneChangeAction laneChangeAction({std::vector<std::string>{"Vehicle1"},
                                                               0.0,
                                                               OpenScenarioEngine::v1_2::TransitionDynamics{},
                                                               []() -> UniqueId { return 0; }},
                                                              {mockEnvironment});

  EXPECT_NO_THROW(laneChangeAction.SetControlStrategy());
  auto control_strategy = dynamic_cast<PerformLaneChangeControlStrategy*>(control_strategies[0].get());
  ASSERT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_NE(nullptr, control_strategy);
  ASSERT_EQ(ControlStrategyType::kPerformLaneChange, control_strategy->type);
  ASSERT_EQ(units::length::meter_t{0.0}, control_strategy->target_lane_offset);
  ASSERT_EQ(UniqueId{0}, control_strategy->target_lane_id);
}