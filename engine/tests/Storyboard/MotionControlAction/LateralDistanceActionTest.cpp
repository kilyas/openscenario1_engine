/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/MotionControlAction/LateralDistanceAction_impl.h"
#include "TestUtils/TestLogger.h"

using namespace mantle_api;

using units::literals::operator""_m;
using units::literals::operator""_rad;

class LateralDistanceActionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::v1_2::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::unique_ptr<testing::OpenScenarioEngine::v1_2::TestLogger> LOGGER;
  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_2::LateralDistanceAction::Values values_{.entities = {"Actor"},
                                                                  .continuous = false,
                                                                  .distance = 0.0,
                                                                  .freespace = false,
                                                                  .lateralDisplacement = mantle_api::LateralDisplacementDirection::kAny,
                                                                  .dynamicConstraints = std::nullopt,
                                                                  .entityRef = "Ego",
                                                                  .coordinateSystem = OpenScenarioEngine::v1_2::CoordinateSystem::kLane};
};

TEST_F(LateralDistanceActionTestFixture, GivenNoDynamicConstraintsAndContinuousFalse_WhenHasControlStrategyGoalBeenReached_ThenReturnsTrue)
{
  OpenScenarioEngine::v1_2::LateralDistanceAction lateralDistanceAction(values_, {mock_environment_});

  ASSERT_TRUE(lateralDistanceAction.HasControlStrategyGoalBeenReached("actorEntity"));
}

TEST_F(LateralDistanceActionTestFixture, GivenNoDynamicConstraintsAndContinuousTrue_WhenHasControlStrategyGoalBeenReached_ThenReturnsFalse)
{
  values_.continuous = true;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateralDistanceAction(values_, {mock_environment_});

  ASSERT_FALSE(lateralDistanceAction.HasControlStrategyGoalBeenReached("actorEntity"));
}

TEST_F(LateralDistanceActionTestFixture, GivenDynamicConstraintsAndContinuousFalse_WhenHasControlStrategyGoalBeenReached_ThenReturnsFalse)
{
  values_.dynamicConstraints = OpenScenarioEngine::v1_2::DynamicConstraintsStruct{};
  OpenScenarioEngine::v1_2::LateralDistanceAction lateralDistanceAction(values_, {mock_environment_});

  ASSERT_FALSE(lateralDistanceAction.HasControlStrategyGoalBeenReached("actorEntity"));
}

TEST_F(LateralDistanceActionTestFixture, GivenContinuousTrue_WhenSetControlStrategy_ThenReturnWithError)
{
  values_.continuous = true;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Continuous action is not supported"));
}

TEST_F(LateralDistanceActionTestFixture, GivenCoordinateSystemOtherThanLane_WhenSetControlStrategy_ThenReturnWithError)
{
  values_.coordinateSystem = OpenScenarioEngine::v1_2::CoordinateSystem::kEntity;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Unsupported coordinate system"));
}

TEST_F(LateralDistanceActionTestFixture, GivenDynamicConstraints_WhenSetControlStrategy_ThenReturnWithError)
{
  values_.dynamicConstraints = OpenScenarioEngine::v1_2::DynamicConstraintsStruct{};
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Found dynamic constraints"));
}

TEST_F(LateralDistanceActionTestFixture, GivenNegativeDistance_WhenSetControlStrategy_ThenExceptionThrown)
{
  values_.distance = -2.0;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  EXPECT_THROW(lateral_distance_action.SetControlStrategy(), std::runtime_error);
}

TEST_F(LateralDistanceActionTestFixture, GivenRefEntityNotOnLane_WhenSetControlStrategy_ThenReturnWithWarning)
{
  std::vector<mantle_api::UniqueId> empty_list{};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService()),
              GetLaneIdsAtPosition(testing::_))
      .WillRepeatedly(testing::Return(empty_list));

  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Reference entity is not on a valid lane"));
}

TEST_F(LateralDistanceActionTestFixture, GivenDistanceWithinRoadNetworkFreespaceFalse_WhenSetControlStrategy_ThenActorPositionIsSet)
{
  // Lateral distance 3.0m
  mantle_api::Vec3<units::length::meter_t> ref_entity_position{1.0_m, 0.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_laterally_shifted_position{1.0_m, 3.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_position{2.0_m, 0.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_laterally_shifted_position{2.0_m, 3.0_m, 0.0_m};
  std::vector<mantle_api::UniqueId> lane_ids{1};

  auto& actor_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("Actor").value().get());
  auto& mock_geometry_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*mock_environment_->GetGeometryHelper());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
      .Times(3)
      .WillOnce(testing::Return(ref_entity_position))
      .WillOnce(testing::Return(actor_entity_position))
      .WillOnce(testing::Return(actor_entity_laterally_shifted_position));

  EXPECT_CALL(mock_query_service,
              GetLaneIdsAtPosition(testing::_))
      .WillRepeatedly(testing::Return(lane_ids));

  EXPECT_CALL(mock_query_service,
              GetProjectedCenterLinePoint(testing::_))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedPoseAtLane(actor_entity_position, testing::_))
      .Times(1)
      .WillOnce(testing::Return(mantle_api::Pose{ref_entity_position, {}}));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(2)
      .WillOnce(testing::Return(ref_entity_laterally_shifted_position))
      .WillOnce(testing::Return(actor_entity_laterally_shifted_position));

  EXPECT_CALL(actor_entity, SetPosition(actor_entity_laterally_shifted_position)).Times(1);

  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();
}

TEST_F(LateralDistanceActionTestFixture, GivenDistanceOutsideRoadNetwork_WhenSetControlStrategy_ThenActorPositionIsNotSet)
{
  std::vector<mantle_api::UniqueId> lane_ids{1};
  auto& actor_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("Actor").value().get());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());

  EXPECT_CALL(mock_query_service, GetLaneIdsAtPosition(testing::_)).WillRepeatedly(testing::Return(lane_ids));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(std::nullopt));

  EXPECT_CALL(actor_entity, SetPosition(testing::_)).Times(0);

  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Desired lateral distance cannot be calculated"));
}

TEST_F(LateralDistanceActionTestFixture, GivenRefEntityPositionWhichCannotBeProjectedToCenterLine_WhenSetControlStrategy_ThenActorPositionIsNotSet)
{
  mantle_api::Vec3<units::length::meter_t> ref_entity_position{1.0_m, 0.0_m, 0.0_m};
  std::vector<mantle_api::UniqueId> lane_ids{1};
  auto& actor_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("Actor").value().get());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());

  EXPECT_CALL(mock_query_service, GetLaneIdsAtPosition(testing::_)).WillRepeatedly(testing::Return(lane_ids));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedCenterLinePoint(testing::_))
      .Times(1)
      .WillOnce(testing::Return(std::nullopt));

  EXPECT_CALL(actor_entity, SetPosition(testing::_)).Times(0);

  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("Desired lateral distance cannot be calculated"));
}

TEST_F(LateralDistanceActionTestFixture, GivenActorEntityPositionWhichCannotBeProjectedToRefEntityCenterLine_WhenSetControlStrategy_ThenActorPositionIsNotSet)
{
  mantle_api::Vec3<units::length::meter_t> ref_entity_position{1.0_m, 0.0_m, 0.0_m};
  std::vector<mantle_api::UniqueId> lane_ids{1};
  auto& actor_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("Actor").value().get());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());

  EXPECT_CALL(mock_query_service, GetLaneIdsAtPosition(testing::_)).WillRepeatedly(testing::Return(lane_ids));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedCenterLinePoint(testing::_))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedPoseAtLane(testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(std::nullopt));

  EXPECT_CALL(actor_entity, SetPosition(testing::_)).Times(0);

  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), testing::HasSubstr("The pose for the entity with name \"Actor\" could not be resolved. "
                                                           "It has not been updated for LateralDistanceAction"));
}

TEST_F(LateralDistanceActionTestFixture, GivenFreespaceTrueVehiclesOrientedSimilarDisplacementLeft_WhenSetControlStrategy_ThenActorPositionIsSet)
{
  // Lateral Distance 5.0m
  // BoundingBox dimensions 2_m X 2_m X 2_m
  mantle_api::Vec3<units::length::meter_t> ref_entity_position{1.0_m, 0.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position{2.0_m, 1.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position_laterally_shifted{2.0_m, 6.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position_projected_to_centerline{2.0_m, 0.0_m, -1.0_m};

  mantle_api::Vec3<units::length::meter_t> actor_entity_position{3.0_m, 2.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontright_corner_position{4.0_m, 1.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontright_corner_position_projected_to_ref_centerline{4.0_m, 0.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontright_corner_position_laterally_shifted{4.0_m, 6.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_target_position{3.0_m, 7.0_m, 0.0_m};

  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{0.0_rad, 0.0_rad, 0.0_rad};
  std::vector<mantle_api::UniqueId> lane_ids{1};

  MockVehicle ref_entity{};
  MockVehicle actor_entity{};
  auto& mock_geometry_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*mock_environment_->GetGeometryHelper());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());
  auto& mock_entity_repository = dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment_->GetEntityRepository());

  EXPECT_CALL(mock_entity_repository, GetImpl("Ego")).WillRepeatedly(testing::Return(&ref_entity));
  EXPECT_CALL(mock_entity_repository, GetImpl("Actor")).WillRepeatedly(testing::Return(&actor_entity));
  EXPECT_CALL(ref_entity, GetPosition()).WillRepeatedly(testing::Return(ref_entity_position));
  EXPECT_CALL(actor_entity, GetPosition()).WillRepeatedly(testing::Return(actor_entity_position));
  EXPECT_CALL(mock_query_service, GetLaneIdsAtPosition(testing::_)).WillRepeatedly(testing::Return(lane_ids));
  EXPECT_CALL(mock_geometry_helper, AreOrientedSimilarly(testing::_, lane_orientation)).WillRepeatedly(testing::Return(true));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(ref_entity_position, testing::_, testing::_))
      .WillRepeatedly(testing::Return(ref_entity_frontleft_corner_position));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(actor_entity_position, testing::_, testing::_))
      .WillRepeatedly(testing::Return(actor_entity_frontright_corner_position));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(actor_entity_frontright_corner_position_laterally_shifted, testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(actor_entity_target_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedCenterLinePoint(ref_entity_frontleft_corner_position))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_frontleft_corner_position_projected_to_centerline));

  EXPECT_CALL(mock_query_service,
              GetProjectedPoseAtLane(actor_entity_frontright_corner_position, testing::_))
      .Times(1)
      .WillOnce(testing::Return(mantle_api::Pose{actor_entity_frontright_corner_position_projected_to_ref_centerline, {}}));

  EXPECT_CALL(mock_query_service,
              GetLaneOrientation(testing::_))
      .WillRepeatedly(testing::Return(lane_orientation));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(2)
      .WillOnce(testing::Return(ref_entity_frontleft_corner_position_laterally_shifted))
      .WillOnce(testing::Return(actor_entity_frontright_corner_position_laterally_shifted));

  EXPECT_CALL(actor_entity, SetPosition(actor_entity_target_position)).Times(1);

  values_.lateralDisplacement = mantle_api::LateralDisplacementDirection::kLeft;
  values_.freespace = true;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();
}

TEST_F(LateralDistanceActionTestFixture, GivenFreespaceTrueVehiclesOrientedOppositeToLaneDisplacementLeft_WhenSetControlStrategy_ThenActorPositionIsSet)
{
  // Lateral Distance 5.0m
  // BoundingBox dimensions 2_m X 2_m X 2_m
  mantle_api::Vec3<units::length::meter_t> ref_entity_position{1.0_m, 0.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position{2.0_m, 1.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position_laterally_shifted{2.0_m, 6.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> ref_entity_frontleft_corner_position_projected_to_centerline{2.0_m, 0.0_m, -1.0_m};

  mantle_api::Vec3<units::length::meter_t> actor_entity_position{3.0_m, 2.0_m, 0.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontleft_corner_position{4.0_m, 3.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontleft_corner_position_projected_to_ref_centerline{4.0_m, 0.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_frontleft_corner_position_laterally_shifted{4.0_m, 6.0_m, -1.0_m};
  mantle_api::Vec3<units::length::meter_t> actor_entity_target_position{3.0_m, 5.0_m, 0.0_m};

  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{0.0_rad, 0.0_rad, 0.0_rad};
  std::vector<mantle_api::UniqueId> lane_ids{1};

  MockVehicle ref_entity{};
  MockVehicle actor_entity{};
  auto& mock_geometry_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*mock_environment_->GetGeometryHelper());
  auto& mock_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());
  auto& mock_entity_repository = dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment_->GetEntityRepository());

  EXPECT_CALL(mock_entity_repository, GetImpl("Ego")).WillRepeatedly(testing::Return(&ref_entity));
  EXPECT_CALL(mock_entity_repository, GetImpl("Actor")).WillRepeatedly(testing::Return(&actor_entity));
  EXPECT_CALL(ref_entity, GetPosition()).WillRepeatedly(testing::Return(ref_entity_position));
  EXPECT_CALL(actor_entity, GetPosition()).WillRepeatedly(testing::Return(actor_entity_position));
  EXPECT_CALL(mock_query_service, GetLaneIdsAtPosition(testing::_)).WillRepeatedly(testing::Return(lane_ids));
  EXPECT_CALL(mock_geometry_helper, AreOrientedSimilarly(testing::_, lane_orientation)).WillRepeatedly(testing::Return(false));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(ref_entity_position, testing::_, testing::_))
      .WillRepeatedly(testing::Return(ref_entity_frontleft_corner_position));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(actor_entity_position, testing::_, testing::_))
      .WillRepeatedly(testing::Return(actor_entity_frontleft_corner_position));

  EXPECT_CALL(mock_geometry_helper,
              TranslateGlobalPositionLocally(actor_entity_frontleft_corner_position_laterally_shifted, testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Return(actor_entity_target_position));

  EXPECT_CALL(mock_query_service,
              GetProjectedCenterLinePoint(ref_entity_frontleft_corner_position))
      .Times(1)
      .WillOnce(testing::Return(ref_entity_frontleft_corner_position_projected_to_centerline));

  EXPECT_CALL(mock_query_service,
              GetProjectedPoseAtLane(actor_entity_frontleft_corner_position, testing::_))
      .Times(1)
      .WillOnce(testing::Return(mantle_api::Pose{actor_entity_frontleft_corner_position_projected_to_ref_centerline, {}}));

  EXPECT_CALL(mock_query_service,
              GetLaneOrientation(testing::_))
      .WillRepeatedly(testing::Return(lane_orientation));

  EXPECT_CALL(mock_query_service,
              GetPosition(testing::_, testing::_, testing::_))
      .Times(2)
      .WillOnce(testing::Return(ref_entity_frontleft_corner_position_laterally_shifted))
      .WillOnce(testing::Return(actor_entity_frontleft_corner_position_laterally_shifted));

  EXPECT_CALL(actor_entity, SetPosition(actor_entity_target_position)).Times(1);

  values_.lateralDisplacement = mantle_api::LateralDisplacementDirection::kLeft;
  values_.freespace = true;
  OpenScenarioEngine::v1_2::LateralDistanceAction lateral_distance_action(values_, {mock_environment_});
  lateral_distance_action.SetControlStrategy();
}
