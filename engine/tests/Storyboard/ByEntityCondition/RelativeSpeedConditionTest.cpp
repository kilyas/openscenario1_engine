/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/RelativeSpeedCondition_impl.h"

using namespace mantle_api;
using units::literals::operator""_mps;

class RelativeSpeedConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_2::RelativeSpeedCondition::Values condition_values_{.triggeringEntity = "vehicle1",
                                                                             .entityRef = "Ego",
                                                                             .rule = OpenScenarioEngine::v1_2::Rule(NET_ASAM_OPENSCENARIO::v1_2::Rule::RuleEnum::EQUAL_TO, 10.0)};
};

TEST_F(RelativeSpeedConditionTestFixture, GivenMockedEntities_WhenCheckingCondition_ThenDoesNotThrow)
{
  OpenScenarioEngine::v1_2::RelativeSpeedCondition relativeSpeedCondition(condition_values_,
                                                                          {mock_environment_});

  EXPECT_NO_THROW([[maybe_unused]] auto _ = relativeSpeedCondition.IsSatisfied());
}

TEST_F(RelativeSpeedConditionTestFixture, GivenSatisfcatoryVelocities_WhenCheckingCondition_ThenReturnsTrue)
{
  OpenScenarioEngine::v1_2::RelativeSpeedCondition relativeSpeedCondition(condition_values_,
                                                                          {mock_environment_});

  // Note: It would be better to have to two distinct ON_CALL statements, one for each entity/MockVehicle to define their respective velocities.
  // However, this is currently not possible, as the MockEntityRepository always returns an empty entity on Get().
  // This could be circumvented by returning a proper entity via the mocked GetImpl() function, but it is not straightforward to create such an entity,
  // as it would have to implement the complete IVehicle interface.

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get(condition_values_.entityRef).value().get()),
              GetVelocity())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{20.0_mps, 0_mps, 0_mps}))
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{10.0_mps, 0_mps, 0_mps}));

  EXPECT_TRUE(relativeSpeedCondition.IsSatisfied());
}

TEST_F(RelativeSpeedConditionTestFixture, GivenUnsatisfcatoryVelocities_WhenCheckingCondition_ThenReturnsFalse)
{
  OpenScenarioEngine::v1_2::RelativeSpeedCondition relativeSpeedCondition(condition_values_,
                                                                          {mock_environment_});

  // Note: It would be better to have to two distinct ON_CALL statements, one for each entity/MockVehicle to define their respective velocities.
  // However, this is currently not possible, as the MockEntityRepository always returns an empty entity on Get().
  // This could be circumvented by returning a proper entity via the mocked GetImpl() function, but it is not straightforward to create such an entity,
  // as it would have to implement the complete IVehicle interface.

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get(condition_values_.entityRef).value().get()),
              GetVelocity())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{15.0_mps, 0_mps, 0_mps}))
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{10.0_mps, 0_mps, 0_mps}));

  EXPECT_THAT(relativeSpeedCondition.IsSatisfied(), false);
}