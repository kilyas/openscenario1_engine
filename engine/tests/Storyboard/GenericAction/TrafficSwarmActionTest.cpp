/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include <optional>
#include <unordered_map>
#include <cstring>

#include "Storyboard/GenericAction/TrafficSwarmAction_impl.h"
#include "TestUtils/MockProbabilityService.h"
#include "Utils/Ellipse.h"

using namespace testing;
using namespace units::literals;

namespace {

struct VehicleParameters
{
  double speed{0.0};
  mantle_api::Vec3<units::length::meter_t> position{};
  mantle_api::Orientation3<units::angle::radian_t> orientation{};
  mantle_api::VehicleClass vehicle_class;
};

} // namespace

class TrafficSwarmActionTestFixture : public ::testing::Test
{
  protected:
    void SetUp() override
    {
      vehicles_.clear();

      auto& mock_entity_repository{dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment_->GetEntityRepository())};
      auto& mock_controller_repository{dynamic_cast<mantle_api::MockControllerRepository&>(mock_environment_->GetControllerRepository())};

      ON_CALL(mock_entity_repository, Create(_, Matcher<const mantle_api::VehicleProperties&>(_))).WillByDefault(
        [this](const std::string& name, const mantle_api::VehicleProperties& properties) -> mantle_api::MockVehicle&
        {
          return CREATE_MOCK_VEHICLE_WITH_PARAMETERIZED_POSITION(name, properties);
        });

      ON_CALL(mock_entity_repository, Delete(Matcher<mantle_api::UniqueId>(_))).WillByDefault(
        [this](mantle_api::UniqueId id)
        {
          const auto& it{vehicles_.find(id)};

          if(it != vehicles_.end())
          {
            vehicles_.erase(it);
            id_list_.erase(std::remove(id_list_.begin(), id_list_.end(), id), id_list_.end());
          }
        });

      ON_CALL(mock_controller_, GetUniqueId()).WillByDefault(Return(1234));

      ON_CALL(mock_controller_repository, Create(_)).WillByDefault(
        [this](std::unique_ptr<mantle_api::IControllerConfig> config) -> mantle_api::MockController&
        {
          controller_count_++;
          last_controller_config_ = dynamic_cast<mantle_api::ExternalControllerConfig&>(*config);
          return mock_controller_;
        });

      ON_CALL(mock_controller_repository, Delete(_)).WillByDefault(
        [this]()
        {
          controller_count_--;
        });

      ON_CALL(*mock_probability_service_, SampleRealUniformValue(_)).WillByDefault(Return(mocked_random_value));

      auto& host{static_cast<mantle_api::MockVehicle&>(mock_entity_repository.GetHost())};

      ON_CALL(host, GetPosition()).WillByDefault(Return(host_position));
      ON_CALL(host, GetOrientation()).WillByDefault(Return(host_orientation));

      OpenScenarioEngine::v1_2::TrafficDefinition traffic_definition;
      traffic_definition.name = "tf";
      traffic_definition.vehicle_category_distribution_entries = {{0.25, mantle_api::VehicleClass::kSmall_car},
                                                                  {0.25, mantle_api::VehicleClass::kMedium_car},
                                                                  {0.25, mantle_api::VehicleClass::kMotorbike},
                                                                  {0.25, mantle_api::VehicleClass::kSemitrailer}};

      mantle_api::ExternalControllerConfig config_1;
      config_1.name = "1";
      mantle_api::ExternalControllerConfig config_2;
      config_2.name = "2";
      mantle_api::ExternalControllerConfig config_3;
      config_3.name = "3";
      mantle_api::ExternalControllerConfig config_4;
      config_4.name = "4";

      traffic_definition.controller_distribution_entries = {{0.25, config_1},
                                                            {0.25, config_2},
                                                            {0.25, config_3},
                                                            {0.25, config_4}};

      parameters_ = OpenScenarioEngine::v1_2::TrafficSwarmActionBase::Values{.innerRadius = 10.0,
                                                                             .numberOfVehicles = 0,
                                                                             .offset = 0.0,
                                                                             .semiMajorAxis = 100.0,
                                                                             .semiMinorAxis = 10.0,
                                                                             .centralObject = {host_name},
                                                                             .trafficDefinition = traffic_definition,
                                                                             .initialSpeedRange = OpenScenarioEngine::v1_2::Range{0.0, 10.0}};
    }

    void RunActionAndSaveDistributions(std::vector<mantle_api::VehicleClass>& vehicle_classes,
                                       std::vector<mantle_api::ExternalControllerConfig>& controller_configs)
    {
      const size_t step_count{100};
      const mantle_api::Vec3<units::length::meter_t> position_offset{500_m, 0_m, 0_m};

      vehicle_classes.reserve(step_count);
      controller_configs.reserve(step_count);

      OpenScenarioEngine::v1_2::TrafficSwarmAction action_under_test_1{parameters_, {mock_environment_}, {mock_probability_service_}};

      for (size_t i{0}; i < step_count; ++i)
      {
        action_under_test_1.Step();
        ASSERT_EQ(1, id_list_.size());
        ASSERT_EQ(1, vehicles_.size());
        ASSERT_EQ(1, controller_count_);

        const auto vehicle_id{id_list_.back()};
        vehicle_classes.push_back(vehicles_.at(vehicle_id).second.vehicle_class);
        controller_configs.push_back(last_controller_config_);

        // positions the vehicle outside of spawning area and forces deletion on next step
        vehicles_.at(vehicle_id).second.position += position_offset;
      }
    }

    const std::string host_name{"host"};
    const mantle_api::Vec3<units::length::meter_t> host_position{10_m, 0_m, 0_m};
    const mantle_api::Vec3<units::velocity::meters_per_second_t> host_velocity{5_mps, 0_mps, 0_mps};
    const mantle_api::Orientation3<units::angle::radian_t> host_orientation{10_rad, 0_rad, 0_rad};

    std::shared_ptr<mantle_api::MockEnvironment> mock_environment_{std::make_shared<mantle_api::MockEnvironment>()};
    mantle_api::MockController mock_controller_;
    std::shared_ptr<OpenScenarioEngine::v1_2::MockProbabilityService> mock_probability_service_{std::make_shared<OpenScenarioEngine::v1_2::MockProbabilityService>()};

    OpenScenarioEngine::v1_2::TrafficSwarmActionBase::Values parameters_;

    std::vector<mantle_api::UniqueId> id_list_;
    std::unordered_map<mantle_api::UniqueId, std::pair<std::unique_ptr<mantle_api::MockVehicle>, VehicleParameters>> vehicles_;
    size_t controller_count_{0};
    mantle_api::ExternalControllerConfig last_controller_config_;

    const double mocked_random_value{0.5};

  private:
    /// Creates a mock vehicle stored in the vehicle_ map.
    /// The vehicle's GetUniqueId and GetPosition methods are mocked and functional.
    /// The vehicle's SetPosition and SetVelocity methods are mocked and functional.
    /// The vehicles parameters are stored in the vehicle_ map.
    /// @param name The name of the vehicle
    /// @param properties The vehicle properties, stored in the vehicle_ map
    /// @return A reference to the created vehicle stored in the vehicle_ map
    ///
    mantle_api::MockVehicle& CREATE_MOCK_VEHICLE_WITH_PARAMETERIZED_POSITION(
        const std::string& name,
        const mantle_api::VehicleProperties& properties)
    {
      const auto num_name{name.substr(std::strlen(name.c_str()) - 1, 1)};
      id_list_.emplace_back(stoul(num_name));
      auto& id{id_list_.back()};

      vehicles_.insert({id, std::make_pair(std::make_unique<mantle_api::MockVehicle>(), VehicleParameters{})});
      auto& mock_vehicle{*(vehicles_.at(id).first)};
      auto& vehicle_parameters{vehicles_.at(id).second};

      vehicle_parameters.vehicle_class = properties.classification;

      ON_CALL(mock_vehicle, GetUniqueId).WillByDefault(Return(id));
      ON_CALL(mock_vehicle, GetPosition).WillByDefault(Return(vehicle_parameters.position));

      ON_CALL(mock_vehicle, SetPosition).WillByDefault([&vehicle_parameters](const mantle_api::Vec3<units::length::meter_t>& position) { vehicle_parameters.position = position; });

      ON_CALL(mock_vehicle, SetVelocity).WillByDefault([&vehicle_parameters](const mantle_api::Vec3<units::velocity::meters_per_second_t>& velocity) { vehicle_parameters.speed = velocity.Length()(); });

      return mock_vehicle;
    }
};

TEST_F(TrafficSwarmActionTestFixture, GivenDeprecatedVelocityParameter_WhenActionIsStepped_ThenExpectThrow)
{
  parameters_.velocity = std::optional<double>(10.0);

  EXPECT_THROW((OpenScenarioEngine::v1_2::TrafficSwarmAction{parameters_, {mock_environment_}, {mock_probability_service_}}), std::runtime_error);
}

TEST_F(TrafficSwarmActionTestFixture, GivenNoRequestedVehicle_WhenActionIsStepped_ThenNoVehicleAreSpawned)
{
  parameters_.numberOfVehicles = 0;
  OpenScenarioEngine::v1_2::TrafficSwarmAction action_under_test{parameters_, {mock_environment_}, {mock_probability_service_}};

  action_under_test.Step();

  EXPECT_EQ(parameters_.numberOfVehicles, vehicles_.size());
  EXPECT_EQ(parameters_.numberOfVehicles, controller_count_);
}

TEST_F(TrafficSwarmActionTestFixture, GivenFiveRequestedVehiclesInFrontOfCentralEntity_WhenActionIsStepped_ThenFiveVehiclesWithCorrectParametersAreSpawned)
{
  const double lower_speed_range_limit{1.0};
  parameters_.numberOfVehicles = 5;
  parameters_.initialSpeedRange = OpenScenarioEngine::v1_2::Range{lower_speed_range_limit, 10.0};
  OpenScenarioEngine::v1_2::TrafficSwarmAction action_under_test{parameters_, {mock_environment_}, {mock_probability_service_}};

  const auto longitudinal_distance_between_vehicles{(parameters_.semiMajorAxis - parameters_.innerRadius) / parameters_.numberOfVehicles};

  std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> spawning_positions;

  for(size_t i{0}; i < parameters_.numberOfVehicles; ++i)
  {
    mantle_api::Pose pose;
    pose.position = host_position;
    pose.position.x += units::length::meter_t(longitudinal_distance_between_vehicles * (i + 1));
    pose.orientation = host_orientation;

    mantle_api::ITrafficSwarmService::SpawningPosition spawning_position{pose, mantle_api::ITrafficSwarmService::RelativePosition::kInFront};

    spawning_positions.push_back(spawning_position);
  }

  EXPECT_CALL(mock_environment_->GetTrafficSwarmService(), GetAvailableSpawningPoses()).Times(1).WillOnce(Return(spawning_positions));

  action_under_test.Step();

  EXPECT_EQ(parameters_.numberOfVehicles, vehicles_.size());
  EXPECT_EQ(parameters_.numberOfVehicles, id_list_.size());
  EXPECT_EQ(parameters_.numberOfVehicles, controller_count_);
  
  for (size_t i{0}; i < id_list_.size(); ++i)
  {
    const auto& id{id_list_[i]};
    EXPECT_GT(vehicles_.count(id), 0);
    EXPECT_NEAR(lower_speed_range_limit + mocked_random_value, vehicles_.at(id).second.speed, 1e-6);
    EXPECT_EQ(host_position.x() + longitudinal_distance_between_vehicles * (i + 1), vehicles_.at(id).second.position.x());
  }
}

TEST_F(TrafficSwarmActionTestFixture, GivenOneRequestedVehiclesOutsideOfSpawningArea_WhenActionIsStepped_ThenVehiclesIsDespawned)
{
  parameters_.numberOfVehicles = 1;
  OpenScenarioEngine::v1_2::TrafficSwarmAction action_under_test{parameters_, {mock_environment_}, {mock_probability_service_}};

  const auto longitudinal_distance_between_vehicles{(parameters_.semiMajorAxis - parameters_.innerRadius) / parameters_.numberOfVehicles};

  std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> spawning_positions;

  for(size_t i{0}; i < parameters_.numberOfVehicles; ++i)
  {
    mantle_api::Pose pose;
    pose.position = host_position;
    pose.position.x += units::length::meter_t(longitudinal_distance_between_vehicles * (i + 1));
    pose.orientation = host_orientation;

    mantle_api::ITrafficSwarmService::SpawningPosition spawning_position{pose, mantle_api::ITrafficSwarmService::RelativePosition::kInFront};

    spawning_positions.push_back(spawning_position);
  }

  EXPECT_CALL(mock_environment_->GetTrafficSwarmService(), GetAvailableSpawningPoses()).Times(2).WillOnce(Return(spawning_positions)).WillOnce(Return(std::vector<mantle_api::ITrafficSwarmService::SpawningPosition>{}));

  action_under_test.Step();

  EXPECT_EQ(parameters_.numberOfVehicles, vehicles_.size());
  EXPECT_EQ(parameters_.numberOfVehicles, id_list_.size());
  EXPECT_EQ(parameters_.numberOfVehicles, controller_count_);

  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment_.get()->GetEntityRepository()), GetEntities).Times(1).WillOnce(
    [this, &vehicle_vec]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      for(const auto& id : id_list_)
      {
        auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
        vehicle_vec.push_back(std::move(vehicle));
        ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(Return(id));
        ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(Return(vehicles_.at(id).second.position));
      }

      return vehicle_vec;
    });
  
  vehicles_.at(id_list_.back()).second.position.x += units::length::meter_t{parameters_.semiMajorAxis};

  action_under_test.Step();

  EXPECT_EQ(0, vehicles_.size());
  EXPECT_EQ(0, controller_count_);
}

TEST_F(TrafficSwarmActionTestFixture, GivenInnerRadiusBiggerThanSpawningArea_WhenActionIsStepped_ThenExpectThrow)
{
  parameters_.innerRadius = 150.0;
  EXPECT_THROW(OpenScenarioEngine::v1_2::TrafficSwarmAction(parameters_, {mock_environment_}, {mock_probability_service_}), std::runtime_error);
}

TEST_F(TrafficSwarmActionTestFixture, GivenTwoActionsWithIdenticalParameters_WhenActionsAreSteppedMultipleTimes_ThenVehicleClassesAndControllerCategoriesAreIdentical)
{
  parameters_.numberOfVehicles = 1;

  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment_->GetEntityRepository()), GetEntities).WillByDefault(
    [this, &vehicle_vec]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      for(const auto& id : id_list_)
      {
        auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
        vehicle_vec.push_back(std::move(vehicle));
        ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(Return(id));
        ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(Return(vehicles_.at(id).second.position));
      }

      return vehicle_vec;
    });

  const std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> spawning_positions{{mantle_api::Pose{host_position, host_orientation}, mantle_api::ITrafficSwarmService::RelativePosition::kInFront}};
  ON_CALL(mock_environment_->GetTrafficSwarmService(), GetAvailableSpawningPoses()).WillByDefault(Return(spawning_positions));

  ON_CALL(mock_environment_->GetTrafficSwarmService(), GetVehicleProperties(_)).WillByDefault(
    [](mantle_api::VehicleClass vehicle_class) -> mantle_api::VehicleProperties
    {
      mantle_api::VehicleProperties properties;
      properties.classification = vehicle_class;
      return properties;
    }
  );

  std::vector<mantle_api::VehicleClass> action_1_vehicle_classes;
  std::vector<mantle_api::VehicleClass> action_2_vehicle_classes;
  std::vector<mantle_api::ExternalControllerConfig> action_1_controller_configs;
  std::vector<mantle_api::ExternalControllerConfig> action_2_controller_configs;

  RunActionAndSaveDistributions(action_1_vehicle_classes, action_1_controller_configs);

  vehicles_.clear();
  id_list_.clear();
  controller_count_ = 0;

  RunActionAndSaveDistributions(action_2_vehicle_classes, action_2_controller_configs);

  EXPECT_EQ(action_1_vehicle_classes, action_2_vehicle_classes);
  EXPECT_EQ(action_1_controller_configs, action_2_controller_configs);
}
