/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByValueCondition/SimulationTimeCondition_impl.h"

using namespace mantle_api;
using units::literals::operator""_s;

class SimulationTimeConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
};

TEST_F(SimulationTimeConditionTestFixture, GivenMockedSimulationTime_WhenCheckingCondition_ThenGetsSimulationTimeAndDoesNotThrow)
{
  auto rule = OpenScenarioEngine::v1_2::Rule(NET_ASAM_OPENSCENARIO::v1_2::Rule::RuleEnum::GREATER_THAN, 1.0);

  OpenScenarioEngine::v1_2::SimulationTimeCondition simulationTimeCondition({rule},
                                                                            {mock_environment_});

  EXPECT_NO_THROW([[maybe_unused]] auto _ = simulationTimeCondition.IsSatisfied());
}

TEST_F(SimulationTimeConditionTestFixture, GivenSatisfactorySimulationTime_WhenCheckingCondition_ThenReturnsTrue)
{
  auto rule = OpenScenarioEngine::v1_2::Rule(NET_ASAM_OPENSCENARIO::v1_2::Rule::RuleEnum::GREATER_THAN, 1.0);

  ON_CALL(*mock_environment_, GetSimulationTime()).WillByDefault(::testing::Return(2.0_s));

  OpenScenarioEngine::v1_2::SimulationTimeCondition simulationTimeCondition({rule},
                                                                            {mock_environment_});

  EXPECT_TRUE(simulationTimeCondition.IsSatisfied());
}

TEST_F(SimulationTimeConditionTestFixture, GivenUnsatisfactorySimulationTime_WhenCheckingCondition_ThenReturnsFalse)
{
  auto rule = OpenScenarioEngine::v1_2::Rule(NET_ASAM_OPENSCENARIO::v1_2::Rule::RuleEnum::GREATER_THAN, 1.0);

  ON_CALL(*mock_environment_, GetSimulationTime()).WillByDefault(::testing::Return(0.0_s));

  OpenScenarioEngine::v1_2::SimulationTimeCondition simulationTimeCondition({rule},
                                                                            {mock_environment_});

  EXPECT_FALSE(simulationTimeCondition.IsSatisfied());
}