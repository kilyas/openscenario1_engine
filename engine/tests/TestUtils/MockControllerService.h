/*******************************************************************************
 * Copyright (c) 2023,
 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "Utils/IControllerService.h"

namespace testing::OpenScenarioEngine::v1_2
{
class MockControllerService : public ::OpenScenarioEngine::v1_2::IControllerService
{
public:
  MOCK_METHOD(std::optional<::OpenScenarioEngine::v1_2::EntityControllers>,
              GetControllers,
              (mantle_api::UniqueId),
              (const, override));
  MOCK_METHOD(mantle_api::UniqueId,
              GetControllerId,
              (std::optional<std::string>, const ::OpenScenarioEngine::v1_2::EntityControllers&),
              (const, override));
  MOCK_METHOD(void,
              ChangeState,
              (mantle_api::UniqueId, mantle_api::UniqueId, mantle_api::IController::LateralState, mantle_api::IController::LongitudinalState),
              (override));
};

}  // namespace testing::OpenScenarioEngine::v1_2