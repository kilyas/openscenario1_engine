/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/RelativeDistanceCondition_impl.h"

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_2
{
bool RelativeDistanceCondition::IsSatisfied() const
{
  if (values.relativeDistanceType == RelativeDistanceType::kLongitudinal &&
      values.coordinateSystem == CoordinateSystem::kEntity)
  {
    const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
    const auto& referencedEntity = EntityUtils::GetEntityByName(mantle.environment, values.entityRef);

    const auto distance = values.freespace
                              ? EntityUtils::CalculateLongitudinalFreeSpaceDistance(
                                    mantle.environment, triggeringEntity, referencedEntity)
                              : EntityUtils::CalculateRelativeLongitudinalDistance(
                                    mantle.environment, triggeringEntity, referencedEntity);

    return values.rule.IsSatisfied(distance.value());
  }

  Logger::Error("RelativeDistanceCondition: Selected relativeDistanceType or coordinateSystem not implemented yet. Only \"longitudinal\" distances and \"entity\" coordinate systems are supported for now. Returning false.");
  return false;
}
}  // namespace OpenScenarioEngine::v1_2