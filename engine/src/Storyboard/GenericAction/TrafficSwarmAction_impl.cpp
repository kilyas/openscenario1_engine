/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/TrafficSwarmAction_impl.h"

#include "MantleAPI/Traffic/entity_properties.h"
#include "MantleAPI/Traffic/i_controller_config.h"
#include "MantleAPI/Traffic/i_traffic_swarm_service.h"
#include "Utils/Ellipse.h"
#include "Utils/EntityUtils.h"

namespace detail
{
constexpr units::length::meter_t ELLIPSE_DETECTION_TOLERANCE{1.0};

mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocityVector(
    units::velocity::meters_per_second_t speed,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
  auto cos_elevation{units::math::cos(orientation.pitch)};
  return {speed * units::math::cos(orientation.yaw) * cos_elevation,
          speed * units::math::sin(orientation.yaw) * cos_elevation,
          speed * -units::math::sin(orientation.pitch)};
}

}  // namespace detail

namespace OpenScenarioEngine::v1_2
{
TrafficSwarmAction::TrafficSwarmAction(Values values, Interfaces interfaces, Services services)
    : TrafficSwarmActionBase{std::move(values), std::move(interfaces), std::move(services)},
      central_entity_{EntityUtils::GetEntityByName(mantle.environment, TrafficSwarmActionBase::values.centralObject)}
{
  const auto& parameters{TrafficSwarmActionBase::values};

  ValidateScenarioParameters(parameters);

  for (const auto& entry : parameters.trafficDefinition.vehicle_category_distribution_entries)
  {
    vehicle_category_distribution_weights_sum_ += entry.weight;
  }

  for (const auto& entry : parameters.trafficDefinition.controller_distribution_entries)
  {
    controller_distribution_weights_sum_ += entry.weight;
  }

  const mantle_api::TrafficSwarmParameters::SpeedRange swarm_speed_range{
      units::velocity::meters_per_second_t(parameters.initialSpeedRange.value().lowerLimit),
      units::velocity::meters_per_second_t(parameters.initialSpeedRange.value().upperLimit)};

  mantle_api::TrafficSwarmParameters swarm_parameters;
  swarm_parameters.central_entity_name = parameters.centralObject;
  swarm_parameters.maximum_number_of_vehicles = parameters.numberOfVehicles;
  swarm_parameters.speed_range = swarm_speed_range;
  swarm_parameters.exclusion_radius = units::length::meter_t(parameters.innerRadius);
  swarm_parameters.semi_minor_spawning_radius = units::length::meter_t(parameters.semiMinorAxis);
  swarm_parameters.semi_major_spawning_radius = units::length::meter_t(parameters.semiMajorAxis);
  swarm_parameters.spawning_area_longitudinal_offset = units::length::meter_t(parameters.offset);

  mantle.environment->InitTrafficSwarmService(swarm_parameters);
}

void TrafficSwarmAction::ValidateScenarioParameters(const OpenScenarioEngine::v1_2::TrafficSwarmActionBase::Values& parameters)
{
  if (parameters.velocity.has_value())
  {
    throw std::runtime_error("TrafficSwarmAction: the \'velocity\' parameter is deprecated. Please use \'initialSpeedRange\' instead.");
  }

  if (!parameters.initialSpeedRange.has_value())
  {
    throw std::runtime_error("TrafficSwarmAction: the \'initialSpeedRange\' parameter has no value. Please adjust the scenario.");
  }

  if (parameters.initialSpeedRange->lowerLimit < 0.0 || parameters.initialSpeedRange->upperLimit < 0.0)
  {
    throw std::runtime_error("TrafficSwarmAction: the \'initialSpeedRange\' limits cannot be negative. Please adjust the scenario.");
  }

  if (parameters.innerRadius > parameters.semiMinorAxis || parameters.innerRadius > parameters.semiMajorAxis)
  {
    throw std::runtime_error("TrafficSwarmAction: the \'innerRadius\' parameter cannot be larger than the \'semiMinorAxis\' or \'semiMajorAxis\' parameters. Please adjust the scenario.");
  }
}

mantle_api::VehicleClass TrafficSwarmAction::GetVehicleClassification()
{
  mantle_api::VehicleClass vehicle_class;
  auto sampled_value{services.sampler->SampleRealUniformValue(vehicle_category_distribution_weights_sum_)};

  for (const auto& vehicle_category_distribution_entry : values.trafficDefinition.vehicle_category_distribution_entries)
  {
    if (sampled_value < vehicle_category_distribution_entry.weight)
    {
      vehicle_class = vehicle_category_distribution_entry.category;
      break;
    }

    sampled_value -= vehicle_category_distribution_entry.weight;
  }

  return vehicle_class;
}

mantle_api::ExternalControllerConfig TrafficSwarmAction::GetControllerConfiguration()
{
  mantle_api::ExternalControllerConfig controller_config;
  auto sampled_value{services.sampler->SampleRealUniformValue(controller_distribution_weights_sum_)};

  for (const auto& controller_distribution_entry : values.trafficDefinition.controller_distribution_entries)
  {
    if (sampled_value < controller_distribution_entry.weight)
    {
      controller_config = controller_distribution_entry.controller;
      break;
    }

    sampled_value -= controller_distribution_entry.weight;
  }

  return controller_config;
}

void TrafficSwarmAction::RemoveEntity(const std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>>::iterator& iterator)
{
  mantle.environment->RemoveEntityFromController(iterator->first, iterator->second);
  mantle.environment->GetEntityRepository().Delete(iterator->first);
  mantle.environment->GetControllerRepository().Delete(iterator->second);
  entity_and_controller_id_list_.erase(iterator);
}

bool TrafficSwarmAction::Step()
{
  DespawnEntities();
  UpdateEnvironment();
  SpawnEntities();

  // A TrafficSwarmAction is an ongoing action and never succeeds
  return false;
}

mantle_api::Pose TrafficSwarmAction::GetSpawningAreaPose() const
{
  mantle_api::Pose spawning_area_pose;
  spawning_area_pose.position = central_entity_.GetPosition();
  spawning_area_pose.orientation = central_entity_.GetOrientation();

  if (values.offset != 0.0)
  {
    const auto lane_pose{mantle.environment->GetQueryService().FindLanePoseAtDistanceFrom(spawning_area_pose,
                                                                                          units::length::meter_t(std::abs(values.offset)),
                                                                                          (values.offset > 0.0 ? mantle_api::Direction::kForward : mantle_api::Direction::kBackwards))};

    if (!lane_pose.has_value())
    {
      throw std::runtime_error(
          "TrafficSwarmAction: can't find the spawning area's pose."
          "The central entity might be too close to the beginning or the end of its lane."
          "Please adjust the \'offset\' parameter or the central entity's position");
    }

    spawning_area_pose = lane_pose.value();
  }

  return spawning_area_pose;
}

void TrafficSwarmAction::DespawnEntities()
{
  if (entity_and_controller_id_list_.empty())
  {
    return;
  }

  const auto& entities{mantle.environment->GetEntityRepository().GetEntities()};
  const auto spawning_area_pose{GetSpawningAreaPose()};

  for (size_t i{0}; i < entity_and_controller_id_list_.size(); ++i)
  {
    const auto entity_iterator{std::find_if(entities.cbegin(), entities.cend(), [this, &i](auto& entity) { return entity_and_controller_id_list_.at(i).first == entity->GetUniqueId(); })};

    if (entity_iterator == std::end(entities))
    {
      entity_and_controller_id_list_.erase(entity_and_controller_id_list_.begin() + i);
      continue;
    }

    const bool entity_is_outside_spawning_area{IsPointOutsideOfEllipse(entity_iterator.base()->get()->GetPosition(),
                                                                                     spawning_area_pose.position,
                                                                                     units::length::meter_t(values.semiMajorAxis),
                                                                                     units::length::meter_t(values.semiMinorAxis),
                                                                                     spawning_area_pose.orientation.yaw,
                                                                                     detail::ELLIPSE_DETECTION_TOLERANCE)};

    if (entity_is_outside_spawning_area)
    {
      RemoveEntity(entity_and_controller_id_list_.begin() + i);
    }
  }
}

units::velocity::meters_per_second_t TrafficSwarmAction::GetSpawnedVehicleSpeed(mantle_api::ITrafficSwarmService::RelativePosition relative_position,
                                                                                double central_entity_speed,
                                                                                double lower_speed_limit,
                                                                                double upper_speed_limit) const
{
  if (relative_position == mantle_api::ITrafficSwarmService::RelativePosition::kInFront && upper_speed_limit > central_entity_speed)
  {
    upper_speed_limit = central_entity_speed;
  }
  else if (relative_position == mantle_api::ITrafficSwarmService::RelativePosition::kBehind && lower_speed_limit < central_entity_speed)
  {
    lower_speed_limit = central_entity_speed;
  }

  return units::velocity::meters_per_second_t{services.sampler->SampleRealUniformValue(upper_speed_limit - lower_speed_limit) + lower_speed_limit};
}

void TrafficSwarmAction::SpawnEntities()
{
  const auto speed_range{values.initialSpeedRange.value()};
  const auto central_entity_speed{central_entity_.GetVelocity().Length().value()};
  const auto spawning_positions{mantle.environment->GetTrafficSwarmService().GetAvailableSpawningPoses()};

  for (const auto& [spawning_pose, relative_position] : spawning_positions)
  {
    const auto vehicle_classification{GetVehicleClassification()};
    const auto vehicle_properties{mantle.environment->GetTrafficSwarmService().GetVehicleProperties(vehicle_classification)};
    const auto vehicle_speed{GetSpawnedVehicleSpeed(relative_position, central_entity_speed, speed_range.lowerLimit, speed_range.upperLimit)};

    auto& entity{mantle.environment->GetEntityRepository().Create("traffic-swarm-entity-" + std::to_string(spawned_entities_count_ + 1), vehicle_properties)};
    entity.SetPosition(spawning_pose.position);
    entity.SetOrientation(spawning_pose.orientation);
    entity.SetVelocity(detail::GetVelocityVector(vehicle_speed, spawning_pose.orientation));

    const auto controller_configuration{GetControllerConfiguration()};
    auto external_controller_config{std::make_unique<mantle_api::ExternalControllerConfig>(controller_configuration)};
    mantle.environment->GetTrafficSwarmService().UpdateControllerConfig(external_controller_config, vehicle_speed);
    const auto& controller{mantle.environment->GetControllerRepository().Create(std::move(external_controller_config))};

    mantle.environment->AddEntityToController(entity, controller.GetUniqueId());

    entity_and_controller_id_list_.push_back({entity.GetUniqueId(), controller.GetUniqueId()});

    spawned_entities_count_++;
  }
}

void TrafficSwarmAction::UpdateEnvironment()
{
  mantle.environment->GetTrafficSwarmService().SetSwarmEntitiesCount(entity_and_controller_id_list_.size());
}

}  // namespace OpenScenarioEngine::v1_2