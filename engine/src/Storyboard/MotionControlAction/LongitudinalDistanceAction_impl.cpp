/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LongitudinalDistanceAction_impl.h"

#include <MantleAPI/Traffic/entity_helper.h>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_2
{
void LongitudinalDistanceAction::SetControlStrategy()
{
  for (const auto& entity : values.entities)
  {
    const auto& refEntity = EntityUtils::GetEntityByName(mantle.environment, values.entityRef);
    auto& actorEntity = EntityUtils::GetEntityByName(mantle.environment, entity);
    auto requested_distance = values.GetLongitudinalDistance();

    if (values.freespace)
    {
      Logger::Warning(
          "LongitudinalDistanceAction: Please note that the \"freespace\" attribute is correct "
          "only for a straight road in the lane coordinate system and when the orientation of both "
          "entities is aligned with the lane centerline.");

      const auto refLength = refEntity.GetProperties()->bounding_box.dimension.length;
      const auto actorLength = actorEntity.GetProperties()->bounding_box.dimension.length;
      requested_distance += refLength.value() / 2 + actorLength.value() / 2;
    }

    const mantle_api::Pose poseRef{refEntity.GetPosition(), refEntity.GetOrientation()};
    if (const auto lanePose = mantle.environment->GetQueryService().FindLanePoseAtDistanceFrom(
            poseRef, units::length::meter_t(requested_distance), values.longitudinalDisplacement))
    {
      // longitudinal distance and lateral shift is calculated using entity ref as object reference.
      // actor pose is currently discarded in the calculations.

      const auto verticalOffset = actorEntity.GetProperties()->bounding_box.dimension.height / 2.0;
      const auto positionOnRoad = mantle.environment->GetQueryService().GetUpwardsShiftedLanePosition(
          lanePose.value().position, verticalOffset.value(), true);
      actorEntity.SetPosition(positionOnRoad);
      actorEntity.SetOrientation(lanePose.value().orientation);
      auto refEntitySpeed = refEntity.GetVelocity().Length();
      mantle_api::SetSpeed(&actorEntity, refEntitySpeed);
    }
    else
    {
      Logger::Warning("LongitudinalDistanceAction: The pose for the entity with name \"" + entity +
                                    "\"  has not been updated for LongitudinalDistanceAction");
    }

    auto control_strategy = std::make_shared<mantle_api::KeepVelocityControlStrategy>();
    const auto entity_id = mantle.environment->GetEntityRepository().Get(entity)->get().GetUniqueId();

    mantle.environment->UpdateControlStrategies(entity_id, {control_strategy});
  }
}

bool LongitudinalDistanceAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
  {
    return (!values.dynamicConstraints.has_value() && !values.continuous);
  }
  return false;
}

mantle_api::MovementDomain LongitudinalDistanceAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kLongitudinal;
}

}  // namespace OpenScenarioEngine::v1_2