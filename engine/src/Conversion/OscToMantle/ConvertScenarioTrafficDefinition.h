
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <MantleAPI/Traffic/entity_properties.h>

#include <string>
#include <vector>

namespace OpenScenarioEngine::v1_2
{

struct VehicleCategoryDistribution
{
  double weight{};
  mantle_api::VehicleClass category{mantle_api::VehicleClass::kOther};
};

struct ControllerDistribution
{
  double weight{};
  mantle_api::ExternalControllerConfig controller{};
};

struct TrafficDefinition
{
  std::string name{};
  std::vector<VehicleCategoryDistribution> vehicle_category_distribution_entries{};
  std::vector<ControllerDistribution> controller_distribution_entries{};
};

TrafficDefinition ConvertScenarioTrafficDefinition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficDefinition>& traffic_definition);

}  // namespace OpenScenarioEngine::v1_2