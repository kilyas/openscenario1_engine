
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <memory>
#include <string>

namespace OpenScenarioEngine::v1_2
{
struct Environment
{
};

inline Environment ConvertScenarioEnvironment(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEnvironment>& /*environment*/)
{
  return {};
}

}  // namespace OpenScenarioEngine::v1_2