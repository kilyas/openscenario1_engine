
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Traffic/control_strategy.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <memory>
#include <optional>
#include <string>

namespace OpenScenarioEngine::v1_2
{
enum class FollowingMode
{
  kFollow,
  kPosition
};

struct TransitionDynamics
{
  mantle_api::TransitionDynamics transitionDynamics;
  std::optional<FollowingMode> followingMode;
};

TransitionDynamics ConvertScenarioTransitionDynamics(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITransitionDynamics>& transitionDynamics);

}  // namespace OpenScenarioEngine::v1_2