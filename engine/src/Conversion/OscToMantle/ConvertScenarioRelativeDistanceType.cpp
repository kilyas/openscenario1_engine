/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_2
{
RelativeDistanceType ConvertScenarioRelativeDistanceType(const NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType& relativeDistanceType)
{
  switch (NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::GetFromLiteral(relativeDistanceType.GetLiteral()))
  {
    case NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::UNKNOWN:
      return RelativeDistanceType::kUnknown;
    case NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::CARTESIAN_DISTANCE:
      return RelativeDistanceType::kCartesian_distance;
    case NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::EUCLIDIAN_DISTANCE:
      return RelativeDistanceType::kEuclidian_distance;
    case NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::LATERAL:
      return RelativeDistanceType::kLateral;
    case NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::LONGITUDINAL:
      return RelativeDistanceType::kLongitudinal;
  }
  throw std::runtime_error("ConvertScenarioRelativeDistanceType: Unknown RelativeDistanceType");
}

}  // namespace OpenScenarioEngine::v1_2