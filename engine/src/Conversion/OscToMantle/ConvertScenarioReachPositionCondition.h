#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <memory>
#include <string>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

namespace OpenScenarioEngine::v1_2
{
struct ReachPositionCondition
{
  double tolerance{};
  std::optional<mantle_api::Pose> pose;
};

inline ReachPositionCondition ConvertScenarioReachPositionCondition(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IReachPositionCondition>& reachPositionCondition)
{
  return {
      reachPositionCondition->GetTolerance(),
      ConvertScenarioPosition(environment, reachPositionCondition->GetPosition())

  };
}

}  // namespace OpenScenarioEngine::v1_2