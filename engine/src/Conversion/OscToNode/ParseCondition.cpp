/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseCondition.h"

#include <memory>

#include "Conversion/OscToMantle/ConvertScenarioConditionEdge.h"
#include "Conversion/OscToNode/ParseByEntityCondition.h"
#include "Conversion/OscToNode/ParseByValueCondition.h"
#include "Node/ConditionNode.h"

namespace detail
{
yase::BehaviorNode::Ptr resolveChildCondition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICondition>& condition)
{
  if (auto element = condition->GetByEntityCondition(); element)
  {
    return OpenScenarioEngine::v1_2::parse(element);
  }
  if (auto element = condition->GetByValueCondition(); element)
  {
    return OpenScenarioEngine::v1_2::parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICondition>");
}
}  // namespace detail

namespace OpenScenarioEngine::v1_2
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICondition> condition)
{
  return std::make_shared<Node::ConditionNode>(
      condition->GetName(),
      condition->GetDelay(),
      ConvertScenarioConditionEdge(condition->GetConditionEdge()),
      detail::resolveChildCondition(condition));
}

}  // namespace OpenScenarioEngine::v1_2