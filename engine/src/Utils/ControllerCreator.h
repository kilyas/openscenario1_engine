/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <memory>

#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_2
{

static constexpr const char* CONTROLLER_NAME_DEFAULT {"OpenScenarioEngine::v1_2::Default"};
static constexpr const char* CONTROLLER_NAME_OVERRIDE {"OpenScenarioEngine::v1_2::ExternalOverride"};

class ControllerService;

class ControllerCreator
{
public:
  explicit ControllerCreator(mantle_api::IEnvironment& environment);
  void CreateControllers(const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IScenarioObject>>& scenario_objects);
  ControllerServicePtr GetControllerService();

  /// Enable override for user defined controllers
  ///
  /// If activated, will enforce creation of an user defined controller
  /// for every entity, which name is "Ego", "Host", or starts with "M1:"
  /// (compatibility for converted dSPACE Model Desk scenarios).
  void EnableUserDefinedControllerOverride() noexcept;

private:
  bool user_defined_controller_override_{false};
  mantle_api::IEnvironment& environment_;
  std::shared_ptr<ControllerService> controller_service_;
};

class ControllerRegistrar
{
public:
  explicit ControllerRegistrar(
      mantle_api::IEntity& entity,
      const std::string& entity_name,
      std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IObjectController>>&& object_controllers,
      mantle_api::IEnvironment& environment,
      mantle_api::IControllerRepository& controller_repository,
      std::shared_ptr<ControllerService>& controller_service);

  void CreateDefaultController();
  void CreateUserDefinedControllers(bool control_override);

private:
  void RegisterDefaultController(mantle_api::IController& controller);
  void RegisterUserDefinedController(mantle_api::IController& controller);
  void RegisterController(mantle_api::IController& controller, bool is_default);

  const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IObjectController>> object_controllers_;
  const std::string& entity_name_;

  mantle_api::IEntity& entity_;
  mantle_api::IEnvironment& environment_;
  mantle_api::IControllerRepository& controller_repository_;
  std::shared_ptr<ControllerService>& controller_service_;
};

}  // namespace OpenScenarioEngine::v1_2
