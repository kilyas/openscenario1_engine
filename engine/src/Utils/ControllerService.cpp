/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/ControllerService.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_2
{
namespace detail
{

template <typename ActivationState>
inline ActivationState invert_if_changed(ActivationState value)
{
  if (value != ActivationState::kNoChange)
  {
    return value == ActivationState::kActivate ? ActivationState::kDeactivate : ActivationState::kActivate;
  }
  return value;
}
}  // namespace detail

std::optional<EntityControllers> ControllerService::GetControllers(
    mantle_api::UniqueId entity_id) const
{
  if (auto iter = controllers.find(entity_id); iter != controllers.end())
  {
    return std::make_optional(iter->second);
  }
  return std::nullopt;
}

mantle_api::UniqueId ControllerService::GetControllerId(
    std::optional<std::string> controller_ref,
    const EntityControllers& entity_controllers) const
{
  const auto& user_defined_controllers = entity_controllers.user_defined;
  if (user_defined_controllers.empty())
  {
    throw std::runtime_error("GetControllerId: No user defined controller defined.");
  }

  if (!controller_ref)
  {
    if (user_defined_controllers.size() > 1)
    {
      throw std::runtime_error("GetControllerId: No controllerRef defined, but multiple controllers available.");
    }
    return user_defined_controllers.begin()->first;
  }

  if (auto iter = mapping.find(*controller_ref); iter != mapping.end())
  {
    return iter->second;
  }

  throw std::runtime_error("GetControllerId: The referenced controller is not available.");
}

void ControllerService::ChangeState(
    mantle_api::UniqueId entity_id,
    mantle_api::UniqueId controller_id,
    mantle_api::IController::LateralState lateral_state,
    mantle_api::IController::LongitudinalState longitudinal_state)
{
  auto entity_controllers = controllers.at(entity_id);
  entity_controllers.internal.second->ChangeState(
      detail::invert_if_changed(lateral_state),
      detail::invert_if_changed(longitudinal_state));
  entity_controllers.user_defined.at(controller_id)->ChangeState(lateral_state, longitudinal_state);
}

}  // namespace OpenScenarioEngine::v1_2