/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <random>

#include "IProbabilityService.h"

namespace OpenScenarioEngine::v1_2
{
class ProbabilityService : public IProbabilityService
{
public:
  explicit ProbabilityService(std::random_device::result_type seed);

  [[nodiscard]] double SampleRealUniformValue(double scale) noexcept override;

private:
  std::random_device::result_type seed_;
  std::mt19937 generator_;
  std::uniform_real_distribution<double> uniform_real_distribution;
};

}  // namespace OpenScenarioEngine::v1_2