/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ProbabilityService.h"

namespace OpenScenarioEngine::v1_2
{
ProbabilityService::ProbabilityService(std::random_device::result_type seed)
    : seed_{seed}, generator_{seed}, uniform_real_distribution{0.0, 1.0} {};

double ProbabilityService::SampleRealUniformValue(double scale) noexcept
{
  return uniform_real_distribution(generator_) * scale;
}

}  // namespace OpenScenarioEngine::v1_2