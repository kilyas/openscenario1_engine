/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/EntityCreator.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <openScenarioLib/generated/v1_2/catalog/CatalogHelperV1_2.h>

#include "Utils/Constants.h"

using namespace units::literals;

namespace OpenScenarioEngine::v1_2
{
std::map<NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::VehicleCategoryEnum, mantle_api::VehicleClass> map_vehicle_class{
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::UNKNOWN, mantle_api::VehicleClass::kOther},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::BICYCLE, mantle_api::VehicleClass::kBicycle},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::BUS, mantle_api::VehicleClass::kBus},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::CAR, mantle_api::VehicleClass::kMedium_car},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::MOTORBIKE, mantle_api::VehicleClass::kMotorbike},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::SEMITRAILER, mantle_api::VehicleClass::kSemitrailer},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::TRAILER, mantle_api::VehicleClass::kTrailer},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::TRAIN, mantle_api::VehicleClass::kTrain},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::TRAM, mantle_api::VehicleClass::kTram},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::TRUCK, mantle_api::VehicleClass::kHeavy_truck},
    {NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory::VAN, mantle_api::VehicleClass::kDelivery_van}};

std::map<NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::PedestrianCategoryEnum, mantle_api::EntityType>
    map_entity_type{{NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::UNKNOWN, mantle_api::EntityType::kOther},
                    {NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::PEDESTRIAN, mantle_api::EntityType::kPedestrian},
                    {NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::ANIMAL, mantle_api::EntityType::kAnimal},
                    {NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::WHEELCHAIR, mantle_api::EntityType::kVehicle}};

EntityCreator::EntityCreator(std::shared_ptr<mantle_api::IEnvironment> environment)
    : environment_(environment) {}

void EntityCreator::CreateEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IScenarioObject> scenario_object)
{
  auto entity_object = scenario_object->GetEntityObject();

  if (!entity_object)
  {
    throw std::runtime_error(std::string("entityObject missing in ScenarioObject " + scenario_object->GetName()));
  }

  if (entity_object->GetVehicle() != nullptr)
  {
    CreateVehicle(entity_object->GetVehicle(), scenario_object->GetName());
  }
  else if (entity_object->GetPedestrian() != nullptr)
  {
    CreatePedestrian(entity_object->GetPedestrian(), scenario_object->GetName());
  }
  else if (entity_object->GetMiscObject() != nullptr)
  {
    CreateMiscObject(entity_object->GetMiscObject(), scenario_object->GetName());
  }
  else if (entity_object->GetCatalogReference() != nullptr)
  {
    CreateCatalogReferenceEntity(entity_object->GetCatalogReference(), scenario_object->GetName());
  }
}

void EntityCreator::CreateVehicle(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IVehicle> vehicle,
                                  const std::string& name)
{
  mantle_api::VehicleProperties properties;
  properties.type = mantle_api::EntityType::kVehicle;
  properties.classification = GetVehicleClass(vehicle->GetVehicleCategory());
  properties.model = vehicle->GetModel3d();
  properties.mass = units::mass::kilogram_t(vehicle->GetMass());
  FillBoundingBoxProperties(properties, vehicle->GetBoundingBox(), vehicle->GetName());
  FillGenericProperties(properties, vehicle->GetProperties()->GetProperties());

  const auto& performance{vehicle->GetPerformance()};

  properties.performance.max_speed = units::velocity::meters_per_second_t(performance->GetMaxSpeed());
  properties.performance.max_acceleration =
      units::acceleration::meters_per_second_squared_t(performance->GetMaxAcceleration());
  properties.performance.max_deceleration =
      units::acceleration::meters_per_second_squared_t(performance->GetMaxDeceleration());

  if (performance->IsSetMaxAccelerationRate())
  {
    properties.performance.max_acceleration_rate =
        units::jerk::meters_per_second_cubed_t(performance->GetMaxAccelerationRate());
  }

  if (performance->IsSetMaxDecelerationRate())
  {
    properties.performance.max_deceleration_rate =
        units::jerk::meters_per_second_cubed_t(performance->GetMaxDecelerationRate());
  }

  FillAxleProperties(properties.front_axle, vehicle->GetAxles()->GetFrontAxle(), vehicle->GetBoundingBox(), name);
  FillAxleProperties(properties.rear_axle, vehicle->GetAxles()->GetRearAxle(), vehicle->GetBoundingBox(), name);

  if (IsDrivingFunctionControlledEntity(name))
  {
    properties.is_host = true;
  }

  environment_->GetEntityRepository().Create(name, properties);
}

void EntityCreator::CreatePedestrian(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IPedestrian> pedestrian,
                                     const std::string& name)
{
  if (pedestrian->GetPedestrianCategory().GetFromLiteral(pedestrian->GetPedestrianCategory().GetLiteral()) ==
      NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory::PedestrianCategoryEnum::WHEELCHAIR)
  {
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.classification = mantle_api::VehicleClass::kWheelchair;
    FillEntityPropertiesForPedestrian(vehicle_properties, pedestrian);
    environment_->GetEntityRepository().Create(name, vehicle_properties);
  }
  else
  {
    mantle_api::PedestrianProperties pedestrian_properties;
    FillEntityPropertiesForPedestrian(pedestrian_properties, pedestrian);
    environment_->GetEntityRepository().Create(name, pedestrian_properties);
  }
}

void EntityCreator::CreateMiscObject(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IMiscObject> misc_object,
                                     const std::string& name)
{
  mantle_api::StaticObjectProperties properties;
  properties.type = mantle_api::EntityType::kOther;
  properties.model = misc_object->GetModel3d();
  FillBoundingBoxProperties(properties, misc_object->GetBoundingBox(), misc_object->GetName());
  FillGenericProperties(properties, misc_object->GetProperties()->GetProperties());
  SetVerticalOffset(properties, misc_object->GetProperties()->GetProperties());
  environment_->GetEntityRepository().Create(name, properties);
}

void EntityCreator::CreateCatalogReferenceEntity(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ICatalogReference> catalog_reference,
    const std::string& name)
{
  auto catalog_element = catalog_reference->GetRef();
  if (catalog_element == nullptr)
  {
    throw std::runtime_error(
        std::string("CatalogReference " + catalog_reference->GetEntryName() + " without ref."));
  }

  if (NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::IsVehicle(catalog_element))
  {
    CreateVehicle(NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::AsVehicle(catalog_element), name);
  }
  else if (NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::IsPedestrian(catalog_element))
  {
    CreatePedestrian(NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::AsPedestrian(catalog_element), name);
  }
  else if (NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::IsMiscObject(catalog_element))
  {
    CreateMiscObject(NET_ASAM_OPENSCENARIO::v1_2::CatalogHelper::AsMiscObject(catalog_element), name);
  }
}

void EntityCreator::FillBoundingBoxProperties(mantle_api::EntityProperties& properties,
                                              std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IBoundingBox> bounding_box,
                                              const std::string& name)
{
  if (!bounding_box)
  {
    throw std::runtime_error(std::string("Entity " + name + " without bounding box."));
  }
  auto dimensions = bounding_box->GetDimensions();
  if (!dimensions)
  {
    throw std::runtime_error(std::string("Bounding box of entity " + name + " without Dimensions."));
  }

  properties.bounding_box.dimension.length = units::length::meter_t(dimensions->GetLength());
  properties.bounding_box.dimension.width = units::length::meter_t(dimensions->GetWidth());
  properties.bounding_box.dimension.height = units::length::meter_t(dimensions->GetHeight());

  auto center = bounding_box->GetCenter();
  if (!center)
  {
    throw std::runtime_error(std::string("Bounding box of entity " + name + " without Center."));
  }

  properties.bounding_box.geometric_center.x = units::length::meter_t(center->GetX());
  properties.bounding_box.geometric_center.y = units::length::meter_t(center->GetY());
  properties.bounding_box.geometric_center.z = units::length::meter_t(center->GetZ());
}

void EntityCreator::FillGenericProperties(
    mantle_api::EntityProperties& entity_properties,
    const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IProperty>>& properties)
{
  for (const auto& property : properties)
  {
    entity_properties.properties.emplace(property->GetName(), property->GetValue());
  }
}

void EntityCreator::FillAxleProperties(mantle_api::Axle& axle,
                                       std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IAxle> open_scenario_axle,
                                       std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IBoundingBox> bounding_box,
                                       const std::string& name)
{
  if (open_scenario_axle == nullptr)
  {
    throw std::runtime_error(std::string("Entity " + name + " is missing axle information."));
  }

  axle.bb_center_to_axle_center = {
      units::length::meter_t(open_scenario_axle->GetPositionX() - bounding_box->GetCenter()->GetX()),
      0.0_m,
      units::length::meter_t(open_scenario_axle->GetPositionZ() - bounding_box->GetCenter()->GetZ())};

  axle.max_steering = units::angle::radian_t(open_scenario_axle->GetMaxSteering());
  axle.track_width = units::length::meter_t(open_scenario_axle->GetTrackWidth());
  axle.wheel_diameter = units::length::meter_t(open_scenario_axle->GetWheelDiameter());
}

void EntityCreator::FillEntityPropertiesForPedestrian(
    mantle_api::EntityProperties& properties,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IPedestrian> pedestrian)
{
  properties.type = GetEntityTypeFromPedestrianCategory(pedestrian->GetPedestrianCategory());
  properties.model = pedestrian->GetModel3d();
  FillBoundingBoxProperties(properties, pedestrian->GetBoundingBox(), pedestrian->GetName());
  FillGenericProperties(properties, pedestrian->GetProperties()->GetProperties());
}

mantle_api::VehicleClass EntityCreator::GetVehicleClass(NET_ASAM_OPENSCENARIO::v1_2::VehicleCategory vehicle_category)
{
  if (map_vehicle_class.find(vehicle_category.GetFromLiteral(vehicle_category.GetLiteral())) !=
      map_vehicle_class.end())
  {
    return map_vehicle_class[vehicle_category.GetFromLiteral(vehicle_category.GetLiteral())];
  }
  throw std::runtime_error(std::string("No vehicle class for vehicle category " + vehicle_category.GetLiteral()));
}

mantle_api::EntityType EntityCreator::GetEntityTypeFromPedestrianCategory(
    NET_ASAM_OPENSCENARIO::v1_2::PedestrianCategory pedestrian_category)
{
  if (map_entity_type.find(pedestrian_category.GetFromLiteral(pedestrian_category.GetLiteral())) !=
      map_entity_type.end())
  {
    return map_entity_type[pedestrian_category.GetFromLiteral(pedestrian_category.GetLiteral())];
  }
  throw std::runtime_error(std::string("No entity type for pedestrian category " + pedestrian_category.GetLiteral()));
}

void EntityCreator::SetVerticalOffset(mantle_api::StaticObjectProperties& entity_properties, const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IProperty>>& properties)
{
  for (const auto& property : properties)
  {
    if (property->GetName() == "mount_height")
    {
      entity_properties.vertical_offset = units::length::meter_t(std::stod(property->GetValue()));
    }
  }
}
}  // namespace OpenScenarioEngine::v1_2
