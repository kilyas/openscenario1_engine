/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_2
{
class ControllerService : public IControllerService
{
public:
  [[nodiscard]] std::optional<EntityControllers> GetControllers(
      mantle_api::UniqueId entity_id) const override;

  [[nodiscard]] mantle_api::UniqueId GetControllerId(
      std::optional<std::string> controller_ref,
      const EntityControllers& entity_controllers) const override;

  void ChangeState(
      mantle_api::UniqueId entity_id,
      mantle_api::UniqueId controller_id,
      mantle_api::IController::LateralState lateral_state,
      mantle_api::IController::LongitudinalState longitudinal_state) override;

  /// Mapping between entity_ids and its controllers
  std::map<mantle_api::UniqueId, EntityControllers> controllers;

  /// Mapping between controller_reference and unique_id of controllers
  std::unordered_map<std::string, mantle_api::UniqueId> mapping;
};

}  // namespace OpenScenarioEngine::v1_2