/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_logger.h>

#include <memory>

namespace OpenScenarioEngine::v1_2
{
class Logger
{
public:
  static void SetLogger(std::shared_ptr<mantle_api::ILogger> logger)
  {
    logger_ = std::move(logger);
  }

  static void Trace(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kTrace, message);
    }
  }

  static void Debug(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kDebug, message);
    }
  }

  static void Info(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kInfo, message);
    }
  }

  static void Warning(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kWarning, message);
    }
  }

  static void Error(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kError, message);
    }
  }

  static void Critical(std::string_view message) noexcept
  {
    if (logger_)
    {
      logger_->Log(mantle_api::LogLevel::kCritical, message);
    }
  }

  static mantle_api::LogLevel GetCurrentLogLevel() noexcept
  {
    if (logger_)
    {
      return logger_->GetCurrentLogLevel();
    }
    return mantle_api::LogLevel::kTrace;
  }

private:
  static std::shared_ptr<mantle_api::ILogger> logger_;
};

}  // namespace OpenScenarioEngine::v1_2