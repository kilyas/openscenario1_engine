/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace OpenScenarioEngine::v1_2::Node
{
class AnyTriggeringEnitityNode : public yase::CompositeNode
{
public:
  explicit AnyTriggeringEnitityNode(const std::string &name = "Unnamed")
      : AnyTriggeringEnitityNode(name, nullptr){};

  explicit AnyTriggeringEnitityNode(yase::Extension::UPtr extension_ptr)
      : AnyTriggeringEnitityNode("Unnamed", std::move(extension_ptr)){};

  AnyTriggeringEnitityNode(const std::string &name, yase::Extension::UPtr extension_ptr)
      : CompositeNode(std::string("Selector::").append(name), std::move(extension_ptr)){};

  ~AnyTriggeringEnitityNode() override = default;

  void onInit() override
  {
    m_last_running_child = -1;
  }

private:
  yase::NodeStatus tick() final
  {
    // Loop over all children
    for (size_t index = 0; index < childrenCount(); index++)
    {
      auto& child = this->child(index);

      // Initialize child before executing it
      if (index != m_last_running_child)
      {
        child.onInit();
      }
      const yase::NodeStatus child_status = child.executeTick();

      switch (child_status)
      {
        case yase::NodeStatus::kRunning:
        {
          if (index != m_last_running_child)
          {
            if (m_last_running_child >= 0)
            {
              this->child(m_last_running_child).onTerminate();
            }
            m_last_running_child = index;   // NOLINT
          }
          return child_status;
        }
        case yase::NodeStatus::kFailure:
        {
          child.onTerminate();
          if (index == m_last_running_child)
          {
            m_last_running_child = -1;
          }
          break;
        }
        case yase::NodeStatus::kSuccess:
        {
          child.onTerminate();
          m_last_running_child = -1;
          return child_status;
        }
        default:
        {
          std::string error_msg = "The child node [";
          error_msg.append(child.name());
          error_msg.append("] returned unkown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }

    // None of the children behavior can handle the situation
    return yase::NodeStatus::kRunning;
  }

  // Index of last running child - negative index indicates none
  int m_last_running_child{-1};
};

}  // namespace OpenScenarioEngine::v1_2::Node