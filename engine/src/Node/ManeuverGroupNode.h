/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <memory>

namespace OpenScenarioEngine::v1_2
{
class EntityBroker;

namespace Node
{
class ManeuverGroupNode : public yase::ParallelNode
{
public:
  ManeuverGroupNode(std::string name,
                    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IActors> actors);

private:
  void lookupAndRegisterData(yase::Blackboard& blackboard) override;

  std::shared_ptr<EntityBroker> entityBroker_;
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IActors> actors_;
};

}  // namespace Node

}  // namespace OpenScenarioEngine::v1_2