/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 Max Paul Bauer - Robert Bosch GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/composite_node.h>
#include <agnostic_behavior_tree/decorator_node.h>

#include <algorithm>
#include <cassert>
#include <memory>
#include <utility>

namespace OpenScenarioEngine::v1_2::Node
{
class TransientNode : public yase::DecoratorNode
{
public:
  using Ptr = std::shared_ptr<TransientNode>;

  explicit TransientNode(const std::string& name)
      : DecoratorNode{name} {};
  ~TransientNode() override = default;

  yase::NodeStatus tick() override
  {
    return child().executeTick();
  }
};

/// @brief assertion helper (see https://stackoverflow.com/a/66269945/7063154)
static bool must_not_be_set(const yase::BehaviorNode::Ptr& child)
{
  return child == nullptr;
}

/// @brief assertion helper (see https://stackoverflow.com/a/66269945/7063154)
static bool must_not_be_set(const yase::BehaviorNode::Ptr& trigger, const yase::BehaviorNode::Ptr& child)
{
  return trigger == nullptr && child == nullptr;
}

/// @brief assertion helper (see https://stackoverflow.com/a/66269945/7063154)
static bool must_be_set(const yase::BehaviorNode::Ptr& child)
{
  return !(must_not_be_set(child));
}

template <typename T>
class StrongNodePtr
{
public:
  explicit StrongNodePtr(yase::BehaviorNode::Ptr node) noexcept // NOLINT(google-explicit-constructor)
      : node{std::move(node)} {};

  static StrongNodePtr<T> Empty()
  {
    return StrongNodePtr(nullptr);
  }

  operator bool() const                                 // NOLINT(google-explicit-constructor)
  {
    return node != nullptr;
  }

  operator yase::BehaviorNode::Ptr &()                        // NOLINT(google-explicit-constructor)
  {
    return node;
  }

private:
  yase::BehaviorNode::Ptr node;
};

using StartTriggerPtr = StrongNodePtr<struct StartTriggerTag>;
using StopTriggerPtr = StrongNodePtr<struct StopTriggerTag>;

/// @brief The TriggerableCompositeNode is used as decorator, oriented on the
///        `Act` and `Storyboard` defined in openSCENARIO 1.0
///
/// This class orients on the `Act` defined in openSCENARIO 1.0, which has
/// a ManeuverGroup, decorated by a StartTrigger and an optional StopTrigger.
/// As such, it can also be used for the `Storyboard`, which only defines a
/// `StopTrigger`, decorating individual `Stories`.
///
/// The Stop- and StartTrigger are BehaviorNodes, stacked before the actual
/// Behavior (child), which allows to define arbitrary complex trees as stop
/// and start condition.
///
/// Its inner structure simulates the following tree
/// └─ StopTrigger     - optional
///    └─ StartTrigger - optional
///       └─ Child     - mandatory
///
/// @note According to the standard, the node never finishes if no StopTrigger
///       is defined, but the inner structure will be ticked every time.
class TriggerableCompositeNode : public yase::CompositeNode
{
public:
  explicit TriggerableCompositeNode(const std::string& name)
      : CompositeNode{name} {};
  ~TriggerableCompositeNode() override = default;

  void onInit() override
  {
    if (!m_child_node)
    {
      throw std::runtime_error("Cannot execute tick. No child set.");
    }

    std::for_each(m_children_nodes.begin(),
                  m_children_nodes.end(),
                  [](auto&& child) { child->onInit(); });
  }

  void set(yase::BehaviorNode::Ptr child,
           StopTriggerPtr stop_trigger = StopTriggerPtr::Empty(),
           StartTriggerPtr start_trigger = StartTriggerPtr::Empty())
  {
    if (m_child_node)
    {
      throw std::runtime_error("set already called");
    }

    // the order of setStopTrigger, setStopTrigger and setChild must not be changed
    // as it fixes how the elements are depicted within the tree

    if (stop_trigger)
    {
      setStopTrigger(stop_trigger);
    }
    if (start_trigger)
    {
      setStartTrigger(start_trigger);
    }
    setChild(std::move(child));
  }

protected:
  using yase::CompositeNode::addChild;

  /// @brief Set the start trigger behavior
  ///
  /// This trigger is executed only if the stop-trigger is unsatisfied,
  /// i.e. returns `running`
  /// @param[in] start_trigger
  void setStartTrigger(yase::BehaviorNode::Ptr start_trigger)
  {
    assert(must_not_be_set(m_child_node));
    setTransientNode("StartTrigger", std::move(start_trigger), start_trigger_node_);
  }

  /// @brief Set the stop trigger behavior
  ///
  /// This trigger is executed until its decorated childs are satisfied,
  /// i.e. return `success`
  /// @param[in] stop_trigger
  void setStopTrigger(yase::BehaviorNode::Ptr stop_trigger)
  {
    assert(must_not_be_set(start_trigger_node_, m_child_node));
    setTransientNode("StopTrigger", std::move(stop_trigger), stop_trigger_node_);
  }

  /// @brief Set the child behavior
  ///
  /// This node is mandatory and is executed if
  /// the start-trigger is satisfied, i.e. returns `success`,
  /// and the stop-trigger is unsatisfied, i.e. returns `running`
  /// @param[in] child
  void setChild(yase::BehaviorNode::Ptr child, const std::string& node_name = "Child")
  {
    setTransientNode(node_name, std::move(child), m_child_node);
  }

  yase::NodeStatus tick() override
  {
    assert(must_be_set(m_child_node));
    return tickWithStopTrigger();
  }

private:
  void setTransientNode(const std::string &name, yase::BehaviorNode::Ptr child, TransientNode::Ptr &member)
  {
    assert(must_not_be_set(member));
    member = std::make_shared<TransientNode>(name);
    member->setChild(std::move(child));
    addChild(member);
  }

  ///@brief tick as if no triggers would decorate the child
  void tickWithoutTriggers()
  {
    const auto status = m_child_node->executeTick();
    if (status == yase::NodeStatus::kFailure)
    {
      throw std::runtime_error("Child reported failure");
    }
  }

  ///@brief tick as if no stop trigger would decorate the start trigger
  void tickWithStartTrigger()
  {
    if (start_trigger_node_ && start_trigger_status != yase::NodeStatus::kSuccess)
    {
      start_trigger_status = start_trigger_node_->executeTick();
      if (start_trigger_status == yase::NodeStatus::kRunning)
      {
        return;
      }
      if (start_trigger_status == yase::NodeStatus::kFailure)
      {
        throw std::runtime_error("StartTrigger reported failure");
      }
    }

    // descent to next hierarchy level
    tickWithoutTriggers();
  }

  ///@brief tick the highest level, i.e. starting with the (optional) stop trigger
  yase::NodeStatus tickWithStopTrigger()
  {
    if (stop_trigger_node_)
    {
      const auto status = stop_trigger_node_->executeTick();
      if (status == yase::NodeStatus::kSuccess)
      {
        return status;
      }
      if (status == yase::NodeStatus::kFailure)
      {
        throw std::runtime_error("StopTrigger reported failure");
      }
    }

    // descent to next hierarchy level
    tickWithStartTrigger();

    // don't consider children on purpose (see class description)
    return yase::NodeStatus::kRunning;
  }

  yase::NodeStatus start_trigger_status{yase::NodeStatus::kIdle};
  TransientNode::Ptr stop_trigger_node_{nullptr};
  TransientNode::Ptr start_trigger_node_{nullptr};
  TransientNode::Ptr m_child_node{nullptr};
};

}  // namespace OpenScenarioEngine::v1_2::Node