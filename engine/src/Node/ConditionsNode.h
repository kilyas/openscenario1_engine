/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace OpenScenarioEngine::v1_2::Node
{
/// Repeatedly execute all its conditions until all are fullfilled
class ConditionsNode : public yase::CompositeNode
{
public:
  /// Creates a conditions node
  explicit ConditionsNode(const std::string &name);
  ~ConditionsNode() override = default;
  void onInit() override;

private:
  yase::NodeStatus tick() final;
};

}  // namespace OpenScenarioEngine::v1_2::Node