# register location of find_scripts
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/deps)

include(CMakeFindDependencyMacro)
find_dependency(Antlr4Runtime)
find_dependency(OpenScenarioAPI)
find_dependency(Yase)
find_dependency(MantleAPI)
find_dependency(units)

include("${CMAKE_CURRENT_LIST_DIR}/OpenScenarioEngineTargets.cmake")
