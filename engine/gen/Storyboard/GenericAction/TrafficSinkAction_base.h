/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <optional>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_2
{
class TrafficSinkActionBase
{
public:
  struct Values
  {
    double radius;
    double rate;
    std::function<std::optional<mantle_api::Pose>()> GetPosition;
    std::optional<TrafficDefinition> trafficDefinition;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TrafficSinkActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};
  virtual ~TrafficSinkActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_2