/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/TrafficStopAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_2::Node
{
class TrafficStopAction : public yase::ActionNode
{
public:
  TrafficStopAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficStopAction> trafficStopAction)
      : yase::ActionNode{"TrafficStopAction"},
        trafficStopAction_{trafficStopAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_2::TrafficStopAction>(
        OpenScenarioEngine::v1_2::TrafficStopAction::Values{
        },
        OpenScenarioEngine::v1_2::TrafficStopAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_2::TrafficStopAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficStopAction> trafficStopAction_;
};

}  // namespace OpenScenarioEngine::v1_2::Node