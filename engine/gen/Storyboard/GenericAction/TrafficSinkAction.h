/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_2/api/ApiClassInterfacesV1_2.h>

#include <cassert>
#include <utility>
#include <limits>

#include "Storyboard/GenericAction/TrafficSinkAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_2::Node
{
class TrafficSinkAction : public yase::ActionNode
{
public:
  TrafficSinkAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficSinkAction> trafficSinkAction)
      : yase::ActionNode{"TrafficSinkAction"},
        trafficSinkAction_{trafficSinkAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_2::TrafficSinkAction>(
        OpenScenarioEngine::v1_2::TrafficSinkAction::Values{
            trafficSinkAction_->GetRadius(),
            trafficSinkAction_->IsSetRate() ? trafficSinkAction_->GetRate() : std::numeric_limits<double>::infinity(),
            [=]() { return ConvertScenarioPosition(environment, trafficSinkAction_->GetPosition()); },
            trafficSinkAction_->IsSetTrafficDefinition()
                ? std::make_optional(ConvertScenarioTrafficDefinition(trafficSinkAction_->GetTrafficDefinition()))
                : std::nullopt},
        OpenScenarioEngine::v1_2::TrafficSinkAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_2::TrafficSinkAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficSinkAction> trafficSinkAction_;
};

}  // namespace OpenScenarioEngine::v1_2::Node