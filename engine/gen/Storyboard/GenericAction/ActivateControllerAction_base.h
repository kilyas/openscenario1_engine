/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/i_controller.h>

#include "Conversion/OscToMantle/ConvertScenarioController.h"
#include "Utils/EntityBroker.h"
#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_2
{
class ActivateControllerActionBase
{
public:
  struct Values
  {
    Entities entities;
    std::optional<bool> animation;
    std::optional<std::string> controllerRef;
    mantle_api::IController::LateralState lateral_state;
    std::optional<bool> lighting;
    mantle_api::IController::LongitudinalState longitudinal_state;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  struct OseServices
  {
    ControllerServicePtr controllerService;
  };

  ActivateControllerActionBase(Values values, Interfaces interfaces, OseServices ose_services)
      : values{std::move(values)},
        mantle{std::move(interfaces)},
        ose_services{std::move(ose_services)} {};
  virtual ~ActivateControllerActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
  OseServices ose_services;
};

}  // namespace OpenScenarioEngine::v1_2