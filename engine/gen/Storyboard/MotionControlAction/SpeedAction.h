/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Storyboard/MotionControlAction/SpeedAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_2::Node
{
class SpeedAction : public yase::ActionNode
{
public:
  SpeedAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ISpeedAction> speedAction)
      : yase::ActionNode{"SpeedAction"},
        speedAction_{speedAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_2::MotionControlAction<OpenScenarioEngine::v1_2::SpeedAction>>(
        OpenScenarioEngine::v1_2::SpeedAction::Values{
            entityBroker->GetEntities(),
            ConvertScenarioTransitionDynamics(speedAction_->GetSpeedActionDynamics()),
            [=]() { return ConvertScenarioSpeedActionTarget(environment, speedAction_->GetSpeedActionTarget()); }},
        OpenScenarioEngine::v1_2::SpeedAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_2::MotionControlAction<OpenScenarioEngine::v1_2::SpeedAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ISpeedAction> speedAction_;
};

}  // namespace OpenScenarioEngine::v1_2::Node