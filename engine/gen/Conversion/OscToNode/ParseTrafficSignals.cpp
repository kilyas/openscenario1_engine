/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseTrafficSignals.h"

#include <units.h>

#include "Node/TrafficSignalPhaseNode.h"
#include "Utils/TrafficSignalBuilder.h"

using namespace units::literals;

namespace OpenScenarioEngine::v1_2
{

TrafficSignalController parse(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficSignalController>& traffic_signal)
{
  static constexpr auto resolve_delay{
      [](const auto& controller) -> std::optional<TrafficSignalController::Delay> {
        if (controller->IsSetDelay() && controller->IsSetReference())  // unhappy paths validated by parser!
        {
          mantle_api::Time delay = units::make_unit<units::time::second_t>(controller->GetDelay());
          return std::nullopt;  // std::make_optional<TrafficSignalController::Delay>(delay, controller->GetReference());
        }
        return std::nullopt;
      }};

  static constexpr auto resolve_phases = [](const auto& phases) -> std::vector<TrafficSignalController::Phase> {
    static constexpr auto resolve_phase = [](const auto& phase) -> TrafficSignalController::Phase {
      static constexpr auto resolve_traffic_signal_states = [](const auto& phase) -> std::vector<std::pair<std::string, std::string>> {
        std::vector<std::pair<std::string, std::string>> states;
        if (auto traffic_signal_states = phase->GetTrafficSignalStates(); !traffic_signal_states.empty())
        {
          states.reserve(traffic_signal_states.size());
          std::transform(
              std::begin(traffic_signal_states),
              std::end(traffic_signal_states),
              std::back_insert_iterator(states),
              [](const auto& traffic_signal_state) -> std::pair<std::string, std::string> {
                return {traffic_signal_state->GetTrafficSignalId(), traffic_signal_state->GetState()};
              });
          return states;
        }
        // remark: TrafficSignalGroupState has been introduced later with 1.2!
        throw std::runtime_error("parseTrafficSignals: TrafficSignalStates not defined.");
      };
      return {
          phase->GetName(),  // potentially introduce name registration service for phases (?)
          units::make_unit<units::time::second_t>(phase->GetDuration()),
          resolve_traffic_signal_states(phase)};
    };

    std::vector<TrafficSignalController::Phase> transformed_phases;
    transformed_phases.reserve(phases.size());
    std::transform(std::begin(phases), std::end(phases), std::back_insert_iterator(transformed_phases), resolve_phase);
    return transformed_phases;
  };

  return {
      std::string(traffic_signal->GetName()),
      resolve_phases(traffic_signal->GetPhases()),
      resolve_delay(traffic_signal)};
}

yase::BehaviorNode::Ptr
parse(const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITrafficSignalController>>& traffic_signals)
{
  std::vector<TrafficSignalController> controllers;
  controllers.reserve(traffic_signals.size());
  std::transform(
      std::begin(traffic_signals),
      std::end(traffic_signals),
      std::back_insert_iterator(controllers),
      [](const auto& traffic_signal) { return parse(traffic_signal); });

  TrafficSignalBuilder builder;
  for (const auto& controller : controllers)
  {
    builder.Add(controller);
  }
  return builder.Create();
}

}  // namespace OpenScenarioEngine::v1_2