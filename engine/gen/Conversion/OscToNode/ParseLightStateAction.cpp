/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseLightStateAction.h"

#include <memory>

#include "Storyboard/GenericAction/LightStateAction.h"

namespace OpenScenarioEngine::v1_2
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ILightStateAction> lightStateAction)
{
  return std::make_shared<Node::LightStateAction>(lightStateAction);
}

}  // namespace OpenScenarioEngine::v1_2