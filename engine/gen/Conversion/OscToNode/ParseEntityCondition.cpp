/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseEntityCondition.h"

#include <memory>

#include "Conversion/OscToNode/ParseAccelerationCondition.h"
#include "Conversion/OscToNode/ParseCollisionCondition.h"
#include "Conversion/OscToNode/ParseDistanceCondition.h"
#include "Conversion/OscToNode/ParseEndOfRoadCondition.h"
#include "Conversion/OscToNode/ParseOffroadCondition.h"
#include "Conversion/OscToNode/ParseReachPositionCondition.h"
#include "Conversion/OscToNode/ParseRelativeClearanceCondition.h"
#include "Conversion/OscToNode/ParseRelativeDistanceCondition.h"
#include "Conversion/OscToNode/ParseRelativeSpeedCondition.h"
#include "Conversion/OscToNode/ParseSpeedCondition.h"
#include "Conversion/OscToNode/ParseStandStillCondition.h"
#include "Conversion/OscToNode/ParseTimeHeadwayCondition.h"
#include "Conversion/OscToNode/ParseTimeToCollisionCondition.h"
#include "Conversion/OscToNode/ParseTraveledDistanceCondition.h"

namespace OpenScenarioEngine::v1_2
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEntityCondition> entityCondition)
{
  if (auto element = entityCondition->GetAccelerationCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetCollisionCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetDistanceCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetEndOfRoadCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetOffroadCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetReachPositionCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetRelativeClearanceCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetRelativeDistanceCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetRelativeSpeedCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetSpeedCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetStandStillCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetTimeHeadwayCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetTimeToCollisionCondition(); element)
  {
    return parse(element);
  }
  if (auto element = entityCondition->GetTraveledDistanceCondition(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEntityCondition>");
}

}  // namespace OpenScenarioEngine::v1_2