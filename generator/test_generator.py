################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from model import Datatype, UmlClass, ChildType

TEST_DATA = {
    "TestNode": {
        "properties": {
            "name": {
                "isList": False,
                "isXorElement": False,
                "type.name": "string",
                "type.isPrimitiveType": True,
                "type.isEnumeration": False
            },
            "rule": {
                "isList": False,
                "isXorElement": False,
                "type.name": "Rule",
                "type.isPrimitiveType": False,
                "type.isEnumeration": True
            },
            "listItems": {
                "isList": True,
                "isXorElement": False,
                "type.name": "ListItem",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            },
            "startTrigger": {
                "isList": False,
                "isXorElement": False,
                "type.name": "Trigger",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            },
            "stopTrigger": {
                "isList": False,
                "isXorElement": False,
                "type.name": "Trigger",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            }
        }
    }
}


def test_model_parser__given_data_with_trigger__parses_trigger_without_setting_as_child():
    uml_class = UmlClass(TEST_DATA, 'TestNode')

    assert uml_class.has_start_trigger == True
    assert uml_class.has_stop_trigger == True

    assert len(uml_class.children()) == 3
    assert len(uml_class.children(ChildType.ENUM)) == 1
    assert len(uml_class.children(ChildType.MODEL)) == 1
    assert len(uml_class.children(ChildType.PRIMITIVE)) == 1


def test_model_parser__given_data__retrieves_name():
    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert uml_class.name == "TestNode"


def test_model_parser__given_data__sets_this_correctly():
    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert uml_class.this.name == "TestNode"
    assert uml_class.this.type == "std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::ITestNode>"
    assert uml_class.this.include('xyz') == '"xyzTestNode.h"'


def test_model_parser__given_choices__are_collected():
    TEST_DATA['TestNode']['properties']['choice1'] = {
        "isList": False,
        "isXorElement": True,
        "type.name": "Choice1",
        "type.isPrimitiveType": False,
        "type.isEnumeration": False
    }
    TEST_DATA['TestNode']['properties']['choice2'] = {
        "isList": False,
        "isXorElement": True,
        "type.name": "Choice2",
        "type.isPrimitiveType": False,
        "type.isEnumeration": False
    }
    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert len(uml_class.children(ChildType.CHOICE)) == 2


def test_child_parser__default_type_without_include__parses_to_stdlib_type_without_include():
    child = Datatype("some", "boolean")
    assert child.name == "Some"
    assert child.type == "bool"
    assert child.include('xyz') is None


def test_child_parser__default_type_with_include__parses_to_stdlib_type_with_include():
    child = Datatype("name", "string")
    assert child.name == "Name"
    assert child.type == "std::string"
    assert child.include('xyz') == "<string>"


def test_child_parser__dateTime__is_parsed_to_string():
    child = Datatype("date", "dateTime")
    assert child.name == "Date"
    assert child.type == "std::string"
    assert child.include('xyz') == "<string>"


def test_child_parser__custom_type__parses_to_parser_type():
    child = Datatype("listItems", "ListItem", is_list=True)
    assert child.name == "ListItems"
    assert child.type == "std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IListItem>>"
    assert child.include('xyz') == '"xyzListItem.h"'
